<?php
include_once("includes/db.php");
include_once("includes/modele.php");
include_once("includes/vue.php");
update_connexion(); // On se connecte si une session est ouverte

// Récupération du pseudo recherché (facultatif)
$pseudo = secure_user_input($_GET["pseudo"]);

if($pseudo)
    entete("Recherche de membre : ".$pseudo);
else
    entete("Liste des membres");

// Affichage du champ de recherche d'un membre
vue_recherche_membre(strtolower($pseudo));
echo '<br/>';

// Affichage de la liste des membres (filtre de recherche compris)
vue_liste_membres(get_liste_membres($pseudo), $pseudo, $LISTE_HIERARCHIES);

pied();
?>