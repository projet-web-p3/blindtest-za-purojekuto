---------------------------
---------------------------
--------- MEMBRES ---------
---------------------------
---------------------------

CREATE TABLE membres(
    id SERIAL PRIMARY KEY,              -- id qui s'incrémente automatiquement
    inscription TIMESTAMP NOT NULL,     -- date et heure de l'inscription
    pseudo VARCHAR(100) NOT NULL,       -- pseudo du membre
    password VARCHAR(100) NOT NULL,     -- mot de passe du membre (algorithme sha1)
    email VARCHAR(100) NOT NULL,        -- mail du membre
    hierarchie INTEGER NOT NULL         -- niveau hiérarchique (0 pour banni, 1 pour membre, 2 pour admin)
);


---------------------------
---------------------------
--------- SESSIONS --------
---------------------------
---------------------------

CREATE TABLE sessions(
    id SERIAL PRIMARY KEY,              -- id qui s'incrémente automatiquement
    user_id INT NOT NULL,               -- id de l'utilisateur pour cette session
    session_id VARCHAR(100) NOT NULL    -- id de session
);



---------------------------
---------------------------
---------- CONTENU ----------
---------------------------
---------------------------

CREATE TABLE contenu(
    id_contenu SERIAL PRIMARY KEY,               -- id qui s'incrémente automatiquement
    url VARCHAR(100) NOT NULL,                   -- chemin pour accéder au contenu
    reponse_attendue VARCHAR(100) NOT NULL,      -- la réponse qui est attendue
    categorie_contenu VARCHAR(100) NOT NULL,     -- le catégorie du contenu
    type_contenu VARCHAR(100) NOT NULL,          -- peut valoir 'image' ou 'musique'
    CONSTRAINT contenu_categorie_unique UNIQUE (url,categorie_contenu) -- permet d'éviter d'ajouter un fichier qui existe déjà pour une certaine catégorie ! (pas toucher !)
);

INSERT INTO contenu VALUES(DEFAULT, 'waiting', 'waiting', 'waiting', 'waiting'); 

---------------------------
---------------------------
--------- PARTIES ---------
---------------------------
---------------------------

CREATE TABLE parties(
    id SERIAL PRIMARY KEY,                                       -- id qui s'incrémente automatiquement
    categorie VARCHAR(100) NOT NULL,                             -- type de musique
    creation TIMESTAMP NOT NULL,                                 -- date où a été crée la partie
    etat INTEGER NOT NULL,                                       -- "attente" = 0 ou "en cours" = 1 ou "finie" = 2 ou "supprimée" = 3
    id_contenu INTEGER REFERENCES contenu(id_contenu) NOT NULL,  -- id du contenu en cours sur la partie
    id_createur INTEGER REFERENCES membres(id) NOT NULL          -- id du créateur de la partie
);


---------------------------
---------------------------
---------- JOUER ----------
---------------------------
---------------------------

CREATE TABLE jouer(
    id_membre INTEGER REFERENCES membres(id),               -- id du membre
    id_game INTEGER REFERENCES parties(id),                 -- id de la game
    ready BOOLEAN NOT NULL,                                 -- le joueur est-il prêt à jouer ?
    score INTEGER NOT NULL,                                 -- score du joueur pour une partie
    a_donne_la_bonne_reponse BOOLEAN NOT NULL,              -- indique si le joueur a répondu vrai pour le contenu actuel
    CONSTRAINT pk_jouer PRIMARY KEY(id_membre, id_game)     -- pour définir la primary key
);





-------------- INSERTION DE MUSIQUES ET DIMAGES POUR LA DEMONSTRATION ---------------
-- Images x10
INSERT INTO contenu VALUES(DEFAULT, './images/1.jpg', 'dora lexploratrice', 'Facile', 'image');
INSERT INTO contenu VALUES(DEFAULT, './images/2.jpg', 'dragon ball', 'Facile', 'image');
INSERT INTO contenu VALUES(DEFAULT, './images/3.jpg', 'goldorak', 'Facile', 'image');
INSERT INTO contenu VALUES(DEFAULT, './images/4.jpg', 'la petite sirène', 'Facile', 'image');
INSERT INTO contenu VALUES(DEFAULT, './images/5.jpg', 'le roi lion', 'Facile', 'image');
INSERT INTO contenu VALUES(DEFAULT, './images/6.jpg', 'olive et tom', 'Facile', 'image');
INSERT INTO contenu VALUES(DEFAULT, './images/7.jpg', 'pokémon', 'Facile', 'image');
INSERT INTO contenu VALUES(DEFAULT, './images/8.jpg', 'superman', 'Facile', 'image');
INSERT INTO contenu VALUES(DEFAULT, './images/9.jpg', 'tarzan', 'Facile', 'image');
INSERT INTO contenu VALUES(DEFAULT, './images/10.jpg', 'winnie lourson', 'Facile', 'image');

-- Musiques x15
INSERT INTO contenu VALUES(DEFAULT, './music/1.mp3', 'aladdin', 'Facile', 'musique');
INSERT INTO contenu VALUES(DEFAULT, './music/2.mp3', 'asterix et la surprise césar', 'Facile', 'musique');
INSERT INTO contenu VALUES(DEFAULT, './music/3.mp3', 'ben 10', 'Facile', 'musique');
INSERT INTO contenu VALUES(DEFAULT, './music/4.mp3', 'candy', 'Facile', 'musique');
INSERT INTO contenu VALUES(DEFAULT, './music/5.mp3', 'capitaine flam', 'Facile', 'musique');
INSERT INTO contenu VALUES(DEFAULT, './music/6.mp3', 'les chevaliers du zodiaque', 'Facile', 'musique');
INSERT INTO contenu VALUES(DEFAULT, './music/7.mp3', 'corneil et bernie', 'Facile', 'musique');
INSERT INTO contenu VALUES(DEFAULT, './music/8.mp3', 'digimon', 'Facile', 'musique');
INSERT INTO contenu VALUES(DEFAULT, './music/9.mp3', 'dora lexploratrice', 'Facile', 'musique');
INSERT INTO contenu VALUES(DEFAULT, './music/10.mp3', 'kim possible', 'Facile', 'musique');
INSERT INTO contenu VALUES(DEFAULT, './music/11.mp3', 'les mystérieuses cités dor', 'Facile', 'musique');
INSERT INTO contenu VALUES(DEFAULT, './music/12.mp3', 'pokémon', 'Facile', 'musique');
INSERT INTO contenu VALUES(DEFAULT, './music/13.mp3', 'tarzan', 'Facile', 'musique');
INSERT INTO contenu VALUES(DEFAULT, './music/14.mp3', 'winx club', 'Facile', 'musique');
INSERT INTO contenu VALUES(DEFAULT, './music/15.mp3', 'les zinzins de lespace', 'Facile', 'musique');