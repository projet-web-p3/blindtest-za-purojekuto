<?php
include_once("includes/includes/db.php");
include_once("includes/modele.php");
include_once("includes/vue.php");
update_connexion(); // On se connecte si une session est ouverte

// On désauthentifie l'utilisateur
$desaut = desauthentification();

// Si l'utilisateur a été banni, il est redirigé ici pour en avoir l'information
if(isset($_GET["ban"]))
    $desaut = 2;

entete("Déconnexion");

if($desaut == 2) // Si l'utilisateur est banni
    affiche_warning("Vous avez été banni. Déconnexion ...");
elseif($desaut == 1) // Si la déconnexion a réussie
    affiche_succes("Déconnexion réussie !");
elseif($desaut == 0) // Pas de connexion avant déconnexion
    affiche_warning("Vous n'êtes pas connecté !");
else // Oups ... problème !
    affiche_erreur("Problème lors de la déconnexion ...");

pied();
?>