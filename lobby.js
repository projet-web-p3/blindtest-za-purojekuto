var id_lobby = 0;
var id_user = 0;

var lobby_status = 0;
var timeout_lobby_status = setInterval(getLobbyStatus, 5000);
var timeout_liste_joueurs = setInterval(reloadJoueurs, 5000); // La liste des joueurs doit être rafraichie périodiquement
var timeout_contenu = setInterval(reloadContenu, 5000); // Le contenu doit être rafraichi périodiquement

var prevent_response = true; // Par défaut, on empêche les réponses

$(document).ready(function() {
    id_lobby = $("#lobby_id_div").attr("value");
    id_user = $("#user_id_div").attr("value");

    $("form#change_status_form").removeAttr("action"); // Si JavaScript actif, on retire l'action du formulaire
    $("form#change_status_form").removeAttr("method"); // On retire aussi la méthode du formulaire

    // Lié au changement de statut d'un joueur
    $("button#btn_change_status").attr("type", "button"); // on change le type du boutton

    disableReponse(); // On ne permet pas la réponse
    getLobbyStatus(); // On récupère l'état actuel du lobby
                
    // Si le contenu est de la musique, on affiche les boutons
    if($("#lobby_refresh_contenu").has("audio").length == 1) {
        $("#music_button").show();
        toggleAudio(true);
    }
    else
        $("#music_button").hide();

    if(lobby_status == 1 || lobby_status == 2)
        enableReponse(); // On permet désormais la réponse


    // Événement lié au clic sur le bouton de début de partie
    $('#button_start_game').click(function(event) {
        if(lobby_status == 0) {
            $("#spinner_start_game").show();
            lobby_status = 1;
            
            // On définit la partie comme ayant commencée
            $.ajax("lobby.php?id=" + id_lobby + "&start").done(displayGameStart);
        }

        event.preventDefault(); // On empêche la redirection
    });


    // Événement lié au clic sur le bouton de fin de partie
    $('#button_stop_game').click(function(event) {
        if(lobby_status != 0) {
            $("#spinner_stop_game").show();
    
            window.clearInterval(timeout_liste_joueurs);
            window.clearInterval(timeout_contenu);
            lobby_status = 3;
    
            // On définit la partie comme ayant terminée
            $.ajax("lobby.php?id=" + id_lobby + "&stop").done(displayGameStop);
        }

        event.preventDefault(); // On empêche la redirection
    });


    // Événement lié au clic sur le bouton de changement de contenu
    $('#button_change_contenu').click(function(event) {
        if(lobby_status != 0) {
            $("#spinner_change_contenu").show();
    
            // On change le contenu sur la page (de façon aléatoire)
            $.ajax("contenu_aleatoire.php?id_lobby=" + id_lobby)
            .done(function(data) {
                reloadContenu();
                $("#reponse").val("");
                enableReponse();
            });
            
        }
    
        event.preventDefault(); // On empêche la redirection
    });


    // Événement lié à l'envoi de la réponse
    $('#reponse_form').submit(function(event) {
        check_reponse(); // On vérifie la réponse
        event.preventDefault(); // On empêche le rafraichissement de page
    });
});


/* Modifications d'affichage liées au début d'une partie */
function displayGameStart() {
    reloadContenu();
    reloadJoueurs();
    
    $("#spinner_start_game").hide();
    $("#lobby_refresh_reponse").show(); // On affiche le formulaire de réponse
    
    $("#button_start_game").hide(); // On masque le bouton pour commencer la partie
    $("#button_change_contenu").show(); // On affiche le bouton pour changer de contenu
    $("#button_stop_game").show(); // On affiche le bouton pour arrêter la partie
}


/* Modifications d'affichage liées à une fin de partie */
function displayGameStop() {
    reloadContenu();
    reloadJoueurs();
    disableReponse();
    
    $("#spinner_stop_game").hide();
    $("#lobby_refresh_reponse").hide(); // On masque le formulaire de réponse

    $("#button_start_game").hide(); // On masque le bouton pour commencer la partie
    $("#button_change_contenu").hide(); // On masque le bouton pour changer de contenu
    $("#button_stop_game").hide(); // On masque le bouton pour arrêter la partie
}


// Ici, on met à jour le contenu affiché sur la page tant que la partie est en cours
function reloadContenu(contenu) {
    var old_content = $('#lobby_refresh_contenu').html();
    
    //if(lobby_status != 3 && lobby_status != 0) {
        $.ajax("refresh_contenu.php?id_lobby=" + id_lobby)
        .done(function(data) {
            
            if(data != old_content) {
                $('#lobby_refresh_contenu').html(data);
                $("#reponse").val("");
                
                // Si le contenu est de la musique, on affiche les boutons
                if($("#lobby_refresh_contenu").has("audio").length == 1) {
                    $("#music_button").show();
                    toggleAudio(true);
                }
                else
                    $("#music_button").hide();
                
                enableReponse(); // On permet la réponse
                reponseFormulaireCouleur(); // Suppresion des couleurs du formulaire de réponse
            }
            
            $("#spinner_change_contenu").hide();
        });
    //}
}


// Ici, on met à jour l'affichage de la liste des membres, que la partie soit en cours ou non
function reloadJoueurs() {
    $('#lobby_refresh_joueurs').load('refresh_liste_joueurs.php?id_lobby='+id_lobby);
}


// Permet de désactiver le formulaire de réponse
function disableReponse(deja_repondu) {
    $("#reponse").prop('disabled', true);

    $("#button_response").prop('disabled', true);
    $("#button_response").addClass("disabled");

    prevent_response = true;
    
    if(deja_repondu)
        reponseFormulaireCouleur("bon");
}


// Permet d'activer le formulaire de réponse
function enableReponse() {
    if(lobby_status == 1 || lobby_status == 2)
        $("#lobby_refresh_reponse").show(); // On affiche le formulaire de réponse

    $("#reponse").prop('disabled', false);

    $("#button_response").prop('disabled', false);
    $("#button_response").removeClass("disabled");

    prevent_response = false;
}


// Ici, on récupère l'état actuel de la partie (en cours ou autre)
function getLobbyStatus() {
    $.ajax("refresh_status.php?id_lobby=" + id_lobby + "&id_user=" + id_user)
    .done(function(data) {
        lobby_status = data; // Vaut : 2 si en cours et bonne réponse, 1 si en cours et mauvaise réponse, et 0 si pas en cours (MODIF PHOKO)

        // Si l'état du lobby est passé à En cours
        if(lobby_status == 1 || lobby_status == 2) {
            displayGameStart();
            if(lobby_status == 1) // On permet la réponse si le joueur n'a pas encore donné de bonne réponse
                enableReponse();
            if(lobby_status == 2) // On ne permet pas la réponse si le joueur a pas déjà donné la bonne réponse
                disableReponse("deja_repondu");
        }
        else if(lobby_status == 3)
            displayGameStop(); // La partie est terminée
        else {
            disableReponse();
            $("#lobby_refresh_reponse").hide();
        }
    });
}


function reponseFormulaireCouleur(type) {
    $("#reponse").parent().parent().removeClass("has-error");
    $("#reponse").parent().parent().removeClass("has-warning");
    $("#reponse").parent().parent().removeClass("has-success");
    
    if(type == "bon")
        $("#reponse").parent().parent().addClass("has-success");

    if(type == "mauvais")
        $("#reponse").parent().parent().addClass("has-error");

    if(type == "chargement")
        $("#reponse").parent().parent().addClass("has-warning");
}


// Permet de vérifier la réponse de l'utilisateur
function check_reponse() {
    // Si l'utilisateur peut effectivement répondre en ce moment
    if(!prevent_response) {
        var xhr = new XMLHttpRequest();
    
        xhr.open("POST", "verif_reponse.php?id_lobby=" + id_lobby + "&id_membre=" + id_user, true);
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.onreadystatechange = function() { // Fonction à appeler lorsque la requête est prête
            if(xhr.readyState == 4 && xhr.status == 200) { // Si tout s'est bien passé
                $("#spinner").hide(); // On masque le spinner

                // Si jamais la réponse est acceptable
                if(xhr.response > 0) {
                    reponseFormulaireCouleur("bon"); // Bonne réponse, affichage en conséquence
                    $("#reponse").val("");
                    disableReponse(); // On empêche de répondre à nouveau
                    reloadJoueurs(); // On raffraichit le tableau des scores
                }

                // Si la réponse est erronnée ou trop mal tapée
                else {
                    reponseFormulaireCouleur("mauvais"); // Mauvaise réponse, affichage en conséquence
                    $("#reponse").val("");
                    enableReponse(); // On permet de répondre à nouveau
                }
            }
        }
        
        xhr.send("reponse=" + $("#reponse").val()); // Envoi de la requête
        reponseFormulaireCouleur("chargement"); // On attend la confirmation de la réponse
        $("#spinner").css("display", "inline-block"); // On affiche le spinner du bouton de réponse
    }
}


// Permet de changer l'état prêt d'un joueur et les affichages qui y sont liés
function change_ready() {
    var xhr = new XMLHttpRequest();

    xhr.open("GET", "change_ready.php?id_user=" + id_user + "&id_lobby=" + id_lobby + "&jsok", true);
    xhr.onreadystatechange = function() { // Fonction à appeler lorsque la requête est prête
        if(xhr.readyState == 4 && xhr.status == 200) { // Si tout s'est bien passé
            $("#btn_change_status").html(xhr.response); // Changement du texte du bouton
            $("#spinner").css("display", "none"); // On masque le spinner
            
            // Suppression des styles du bouton
            $("#btn_change_status").removeClass("btn-danger");
            $("#btn_change_status").removeClass("btn-success");
            $("#btn_change_status").removeClass("btn-warning");
            
            if(xhr.response == "Prêt")
                $("#btn_change_status").addClass("btn-success");
            if(xhr.response == "Indisponible")
                $("#btn_change_status").addClass("btn-danger");
                
        }
    }
    xhr.send(); // Envoi de la requête
    
    // Changement des styles et contenus pour refléter le chargement de la requête
    $("#btn_change_status").removeClass("btn-danger");
    $("#btn_change_status").removeClass("btn-success");
    $("#btn_change_status").addClass("btn-warning");
    
    $("#btn_change_status").html("Mise à jour ...");
    $("#spinner").css("display", "inline-block");
}


function playAudio() {
    document.getElementById("myAudio").play();
}

function pauseAudio() {
    document.getElementById("myAudio").pause();
}

var audio_playing = true;
function toggleAudio(force_audio = false) {
    $("#music_button").removeClass("btn-warning");
    $("#music_button").removeClass("btn-success");
    $("#music_button span").removeClass("glyphicon-pause");
    $("#music_button span").removeClass("glyphicon-play");
    
    if(!audio_playing || force_audio) {
        $("#myAudio")[0].play();
        $("#music_button").addClass("btn-warning");
        $("#music_button span").addClass("glyphicon-pause");
        audio_playing = true;
    }
    else {
        $("#myAudio")[0].pause();
        $("#music_button").addClass("btn-success");
        $("#music_button span").addClass("glyphicon-play");
        audio_playing = false;
    }
    
}