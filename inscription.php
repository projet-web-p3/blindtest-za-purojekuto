<?php
include_once("includes/db.php");
include_once("includes/modele.php");
include_once("includes/vue.php");
update_connexion(); // On se connecte si une session est ouverte

entete("Inscription");

// On empêche l'inscription si on est déjà connecté
if(verif_connexion())
    affiche_warning("Vous êtes déjà connecté !");
else {
    // Récupération des valeurs des champs du formulaire d'inscription
    $pseudo = secure_user_input($_POST["pseudo"]);
    $email = secure_user_input($_POST["email"]);
    $password = $_POST["password"];
    $conf_password = $_POST["conf_password"];
    
    // Si un formulaire d'inscription a été rempli
    if(isset($_POST["inscription_form"])) {
        // Tableau stockant les diverses erreurs possibles
        $errors["empty_pseudo"] = empty($pseudo); // Pseudo fourni
        $errors["empty_email"] = empty($email); // Mail fourni
        $errors["empty_password"] = empty($password); // Mot de passe fourni
        $errors["empty_conf_password"] = empty($conf_password); // Confirmation du mot de passe fournie

        if($pseudo)
            $errors["failure_pseudo"] = !is_unique_pseudo($pseudo); // Vérification de l'unicité du pseudo
        
        if($email) {
            $errors["failure_email"] = !filter_var($email, FILTER_VALIDATE_EMAIL); // Vérification du format de l'adresse mail
            $errors["failure_email_exists"] = !is_unique_mail($email); // Vérification de l'unicité de l'adresse mail
        }
        
        if($password && $conf_password)
            $errors["failure_pwd"] = ($password != $conf_password); // Confirmation du mot de passe
    
        // Si tout est bon, on rajoute un nouveau membre
        if(!has_error($errors)) {
            ajout_membre($pseudo, sha1($password), $email);
            affiche_succes('Inscription réussie ! <a href="connexion.php">Connexion ...</a>');
        }
        else
            vue_inscription($errors, $pseudo, $email);
    }
    else
        vue_inscription($errors);
}

pied();
?>