<?php
include_once("includes/db.php");
include_once("includes/modele.php");
include_once("includes/vue.php");
update_connexion(); // On se connecte si une session est ouverte

if(!verif_connexion()) // Si l'utilisateur n'est pas connecté
    affiche_erreur("Vous n'êtes plus connecté.");
else { // Si l'utilisateur est connecté
    if(!isset($_GET["id_lobby"])) // Si aucun ID lobby passé en paramètre
        affiche_erreur("Un problème est survenu.");
    elseif(!ctype_digit($_GET["id_lobby"])) // Si l'ID passé en paramètre n'est pas un entier
        affiche_erreur("Un problème est survenu.");
    else { // Si un ID de lobby est bien passé en paramètre
        $id_lobby = $_GET["id_lobby"];
        
        if($lobby = get_lobby_contenu($id_lobby)) // Si le lobby existe
            if(!is_lobby_supprime($lobby)) // Si le lobby n'est pas supprimé
                vue_liste_joueurs(get_liste_joueurs($id_lobby), $lobby, $membre_connecte, 1);
    }
}
?>