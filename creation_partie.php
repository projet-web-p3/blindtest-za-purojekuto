<?php
include_once("includes/db.php");
include_once("includes/modele.php");
include_once("includes/vue.php");
update_connexion(); // On se connecte si une session est ouverte

entete("Création d'une partie");

if(!verif_connexion()) // Si l'utilisateur n'est pas connecté
    affiche_warning("Vous n'êtes pas connecté !");
elseif(!isset($_POST["categorie"])) // Si aucune catégorie n'a été définie
    affiche_erreur("Pour créer une partie, veuillez vous rendre sur la page d'accueil.");
else {
    if(liste_lobbys(array(0,1), $membre_connecte["id"])) // S'il existe déjà une partie "En attente" ou "En cours" par cet utilisateur
        affiche_erreur("Une partie créée par vous est déjà en attente ou en cours ! Veuillez d'abord la supprimer avant d'en créer une autre.");
    else { // Si aucune partie liée à l'utilisateur n'est en attente ou en cours
        $categorie = secure_user_input($_POST["categorie"]); // Catégorie du lobby à créer

        if(!in_array($categorie, $LISTE_CATEGORIES)) // Si la catégorie n'existe pas
            affiche_erreur("Cette catégorie n'existe pas.");
        else { // Si la catégorie existe
            $id_lobby = creer_lobby($membre_connecte["id"], $categorie); // On créée le lobby
            affiche_succes("Partie créée !");
            affiche_info("Clique <a href='lobby.php?id=".$id_lobby."'>ici</a> pour rejoindre la partie que tu viens de créer !");
        }
    }
}

pied();
?>