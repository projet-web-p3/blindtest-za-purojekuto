<?php
include_once("includes/db.php");
include_once("includes/modele.php");
include_once("includes/vue.php");
update_connexion(); // On se connecte si une session est ouverte

// Récupération de l'ID du membre à gérer
if(isset($_GET["id"]))
    if(ctype_digit($_GET["id"]))
        $id = $_GET["id"];

// Si aucun ID valide n'a été fourni
if(!$id) {
    entete("Gestion de membre");
    affiche_erreur("Aucun ID de membre valide n'a été fourni.");
}
// Si un ID valide de membre a été fourni
else {
    // Si l'utilisateur est connecté
    if(verif_connexion()) {
        // Si l'utilisateur connecté est un admin
        if(is_membre_admin($membre_connecte)) {
            // Si l'utilisateur essaye de se gérer lui-même
            if($id == $membre_connecte["id"]) {
                entete("Gestion de membre");
                affiche_erreur("Vous ne pouvez pas gérer votre propre compte !");
            }
            // Si cet ID de membre correspond à un membre existant
            elseif($membre = get_membre($id)) {
                // Cas du banissement d'un membre
                if(isset($_GET["ban"])) {
                    entete("Banissement d'un membre");
                    // On vérifie qu'on peut bannir ce membre (ie. le membre n'est pas un administrateur)
                    if(!is_membre_admin($membre)) {
                        if(set_membre_banni($membre))
                            affiche_succes($membre["pseudo"]." a été banni.");
                        else
                            affiche_erreur("Une erreur est survenue lors du banissement de ".$membre["pseudo"]);
                    }
                    else
                        affiche_erreur("Vous ne pouvez pas bannir ".$membre["pseudo"]." car c'est un administrateur.");
                }
                // Cas du pardon d'un membre
                elseif(isset($_GET["unban"])) {
                    entete("Pardon d'un membre");
                    if(set_membre_simple($membre))
                        affiche_succes($membre["pseudo"]." a été pardonné.");
                    else
                        affiche_erreur("Une erreur est survenue lors du pardon de ".$membre["pseudo"]);
                }
                // Si aucune action n'a été fournie
                else {
                    entete("Gestion de membre");
                    affiche_warning("Aucune action n'a été indiquée.");
                }
            }
            // Si aucun membre ne correspond à cet ID
            else {
                entete("Gestion de membre");
                affiche_erreur("Aucun utilisateur associé à cet ID n'a été trouvé.");
            }
        }
        // Si l'utilisateur connecté n'est pas un admin
        else {
            entete("Gestion de membre");
            affiche_erreur("Vous n'êtes pas administrateur et ne pouvez donc pas accéder à la gestion de membre.");
        }
    }
    // Si l'utilisateur n'est pas connecté
    else {
        entete("Gestion de membre");
        affiche_erreur("Vous n'êtes pas connecté.");
    }
}

pied();
?>