<?php
require_once(__DIR__ . '/../vendor/autoload.php');
include_once("includes/modele.php");
include_once("includes/vue.php");
update_connexion(); // On se connecte si une session est ouverte

entete("Accueil");

if(verif_connexion()) { // Si l'utilisateur est connecté, on lui propose de créer une partie
    echo '<h3>Création d\'une partie</h3>';
    
    if(liste_lobbys(array(0,1), $membre_connecte["id"])) // S'il existe déjà une partie "en attente" ou "en cours" par cet utilisateur
        affiche_info("Une partie créée par vous est déjà en attente ou en cours ! Veuillez d'abord attendre qu'elle finisse ou la supprimer avant d'en créer une autre.");
    else // Sinon on lui propose de créer une partie
        vue_creation_partie($LISTE_CATEGORIES); // On affiche le formulaire pour créer une partie
}
else
    affiche_info("Connectez ou inscrivez-vous pour participer à une partie !");

// Affichage de la liste des lobbys
echo '<h3>Parties disponibles</h3>';
vue_liste_lobbys(liste_lobbys(), $LISTE_ETATS_LOBBYS);

pied();
?>