<?php
include_once("includes/db.php");
include_once("includes/modele.php");
include_once("includes/vue.php");
update_connexion(); // On se connecte si une session est ouverte

// On empêche la connexion si on est déjà connecté
if(verif_connexion()) {
    entete("Connexion");
    affiche_warning("Vous êtes déjà connecté !");
}
else {
    // Récupération des valeurs des champs du formulaire de connexion
    $pseudo = secure_user_input($_POST["pseudo"]);
    $password = $_POST["password"];
    $remember = ($_POST["remember"] == "on") ? 1 : 0;

    $membre = get_membre(0, $pseudo); // On récupère le membre (si il existe) associé au pseudo

    // Si un formulaire de connexion a été rempli
    if(isset($_POST["connexion_form"])) {
        // Tableau stockant les diverses erreurs possibles
        $errors["empty_pseudo"] = empty($pseudo); // Pseudo fourni
        $errors["empty_password"] = empty($password); // Mot de passe fourni

        if($pseudo) // Si le pseudo n'existe pas
            $errors["failure_pseudo"] = ($membre["id"]) ? 0 : 1;

        if($password && $membre["id"]) // Si le mot de passe ne correspond pas
            $errors["failure_pwd"] = !verif_mdp($membre["id"], $password);

        // Si tout est bon, on essaye de connecter l'utilisateur en vérifiant s'il n'est pas banni
        if(!has_error($errors)) {
            if(is_membre_banni($membre)) { // Si jamais l'utilisateur est banni
                entete("Connexion");
                affiche_erreur("Vous ne pouvez pas vous connecter : vous êtes banni.");
            }
            else { // Sinon, on le connecte
                authentification($membre["id"], $remember);
                entete("Connexion");
                affiche_succes("Connexion réussie !");
            }
        }
        else {
            entete("Connexion");
            vue_connexion($errors, $pseudo);
        }
    }
    else {
        entete("Connexion");
        vue_connexion($errors);
    }
}

pied();
?>