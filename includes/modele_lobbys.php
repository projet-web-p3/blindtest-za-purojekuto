<?php
/* Récupère les informations du lobby
 * @param: $id l'id du lobby
 * @return: un tableau contenant les informations du lobby, vide sinon
 */
function get_lobby($id) {
    $id = intval($id); // Permet d'éviter d'avoir des chaînes à la place d'un entier
    
    $db = db_connect();
    $rep = db_query($db, "SELECT * FROM parties WHERE id = ".$id.";");
    db_close($db);
    $fetch = db_fetch($rep);
    
    if(count($fetch) == 1)
        return $fetch[0];
    return [];
}

/* Récupère les informations de la partie et du contenu associé
 * @param: $id_lobby l'id du lobby
 * @return: un tableau contenant les informations du contenu joué dans ce lobby et du lobby lui-même
 */
function get_lobby_contenu($id_lobby) {
    $id_lobby = intval($id_lobby); // Permet d'éviter d'avoir des chaînes à la place d'un entier
    
    $db = db_connect();
    $rep = db_query($db, "SELECT * FROM parties INNER JOIN contenu ON contenu.id_contenu = parties.id_contenu WHERE parties.id = ".$id_lobby.";");
    db_close($db);
    $fetch = db_fetch($rep);
    
    if(count($fetch) == 1)
        return $fetch[0];
    return [];
}

/* Récupère la liste des lobbys
 * @param: $etats les états de lobby recherchés sous forme [0, 1, 2, 3]
 * @return: un tableau contenant les informations des lobbys, vide sinon
 */
function liste_lobbys($etats = array(), $id_createur = 0) {
    // Limitation des états recherchés
    $etats_recherche = "";
    $delimiteur = "";
    if($etats) {
        $etats_recherche = "WHERE (";
        foreach($etats as $etat) {
            $etats_recherche .= $delimiteur." etat = ".$etat;
            $delimiteur = " OR";
        }
        $etats_recherche .= ")";
    }
    
    // Si on a précisé $id_membre en paramètre, on le rajoute à la requête
    $membre_recherche = "";
    if($id_createur)
        $membre_recherche = " AND id_createur = ".$id_createur;
    
    $db = db_connect();
    $rep = db_query($db, "SELECT * FROM parties ".$etats_recherche . $membre_recherche." ORDER BY creation DESC;");
    db_close($db);
    $fetch = db_fetch($rep);
    
    if(count($fetch) > 0)
        return $fetch;
    return [];
}

/* Création d'un lobby
 * @param: $id_membre l'id du membre créateur du lobby
 * @param: $categorie la catégorie de la partie qui sera jouée
 * @return: l'id de la partie qu'on vient de créer
 */
function creer_lobby($id_membre, $categorie) {
    $db = db_connect();
    $rep = db_query($db, "INSERT INTO parties VALUES(DEFAULT, '" .$categorie. "', current_timestamp, 0, 1, " .$id_membre. ") RETURNING id;");
    db_close($db);
    
    $id_lobby = db_fetch($rep)[0]["id"]; 
    $lobby = get_lobby($id_lobby);
    change_content($lobby); // On choisi un contenu aléatoire pour commencer le lobby
    
    return $id_lobby;
}

/* Supprime le lobby correspondant à l'id passé en paramètre
 * @param: $lobby les informations du lobby à supprimer
 */
function suppression_lobby($lobby) {
    $db = db_connect();
    set_lobby_supprime($lobby);
    $rep = db_query($db, "DELETE FROM jouer WHERE id_game = ".$id.";");
    db_close($db);    
}

/* Vérifie si un membre est créateur du lobby ou non
 @param: $lobby le tableau des informations du lobby dont on veut vérifier qu'il est créateur
 @param: $id_membre l'id du membre
 @return: true si le lobby a été crée par ce membre, false sinon
 */
function is_creator_of_lobby($lobby, $id_membre) {
    return ($lobby["id_createur"] == $id_membre);
}


/*----------------------------------------------
---- FONCTIONS DE TESTS DE L'ETAT DU LOBBY -----
----------------------------------------------*/


/* Test de l'état "En attente" du lobby
 @param: $lobby le tableau des informations du lobby
 @return: true si le lobby est en attente, false sinon
 */
function is_lobby_attente($lobby) {
    return $lobby["etat"] == 0;
}

/* Test de l'état "En cours" du lobby
 @param: $lobby le tableau des informations du lobby
 @return: true si le lobby est en cours, false sinon
 */
function is_lobby_encours($lobby) {
    return $lobby["etat"] == 1;
}

/* Test de l'état "Fini" du lobby
 @param: $lobby le tableau des informations du lobby
 @return: true si le lobby est fini, false sinon
 */
function is_lobby_fini($lobby) {
    return $lobby["etat"] == 2;
}

/* Test de l'état "Supprimé" du lobby
 @param: $lobby le tableau des informations du lobby
 @return: true si le lobby est supprimé, false sinon
 */
function is_lobby_supprime($lobby) {
    return $lobby["etat"] == 3;
}


/*----------------------------------------------
-------- CHANGEMENT DE L'ETAT DU LOBBY ---------
----------------------------------------------*/


/* Mets à jour la BDD en changeant l'état d'un lobby
 @param: $lobby le tableau des informations du lobby
 @param: $etat le nouvel état du lobby
 @return: le nombre de lignes affectées par la requête UPDATE
 */
function set_etat_lobby(&$lobby, $etat = 1) {
    $db = db_connect();
    $rep = db_query($db, "UPDATE parties SET etat = ".$etat." WHERE id = ".$lobby["id"].";");
    db_close($db);
    
    $lobby["etat"] = $etat;

    if($rep)
        return count(db_fetch($rep));
    return 1;
}

/* Mets à jour la BDD en rendant un lobby "En attente" */
function set_lobby_attente(&$lobby) {
    return set_etat_lobby($lobby, 0);
}

/* Mets à jour la BDD en rendant un lobby "En cours" */
function set_lobby_encours(&$lobby) {
    return set_etat_lobby($lobby, 1);
}

/* Mets à jour la BDD en rendant un lobby "Fini" */
function set_lobby_fini(&$lobby) {
    return set_etat_lobby($lobby, 2);
}

/* Mets à jour la BDD en rendant un lobby "Supprimé" */
function set_lobby_supprime(&$lobby) {
    return set_etat_lobby($lobby, 3);
}
?>