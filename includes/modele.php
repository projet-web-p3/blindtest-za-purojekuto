<?php
session_start();
include_once("includes/db.php");

include_once("includes/modele_membres.php");
include_once("includes/modele_lobbys.php");
include_once("includes/modele_joueurs.php");

$LISTE_HIERARCHIES = array("Banni", "Membre", "Administrateur"); // liste des niveaux hiérarchiques
$LISTE_CATEGORIES = array("Facile", "Moyen", "Difficile"); // liste des catégories qui existent
$LISTE_ETATS_LOBBYS = array("En attente", "En cours de jeu", "Finie", "Supprimée"); // liste des états d'un lobby


/* Vérifie si l'utilisateur est connecté ou non
 * @return: true si l'utilisateur est connecté, false sinon
 */
function verif_connexion() {
    if(isset($_SESSION["auth"]))
        return ($_SESSION["auth"] == true);
    return false;
}

/* Connecte l'utilisateur si une session est déjà ouverte */
function update_connexion() {
    // Récupération de l'éventuelle session d'un utilisateur
    if(isset($_COOKIE['user'])) {
        $db = db_connect();
        $rep = db_query($db, "SELECT * FROM sessions WHERE session_id = '".$_COOKIE['user']."';");
        db_close($db);
        
        if(db_count($rep) == 1) { // Le cookie correspond à une session côté serveur
            $_SESSION["auth"] = true;
            $_SESSION["user_id"] = db_fetch($rep)[0]["user_id"];
        }
        else // Le cookie ne correspond à aucune session côté serveur
            desauthentification();
    }
    
    global $membre_connecte;
    $membre_connecte = get_connected();
    
    // Si l'utilisateur est banni
    if(verif_connexion())
        if(is_membre_banni($membre_connecte))
            header('Location: deconnexion.php?ban'); // Renvoi sur la page de déconnexion
}

/* Authentification d'un utilisateur
 * @param: $id l'id du membre (non nul)
 * @param: $remember vaut 1 si l'utilisateur veut rester connecté, 0 sinon
 */
function authentification($id, $remember) {
    $_SESSION["auth"] = true;
    $_SESSION["user_id"] = $id;
    
    if($remember) { // On sauvegarde la session de l'utilisateur pour 1 an
        setcookie('user', session_id(), time() + 3600*24*365);
        
        // Ajout de la session à la base sessions
        $db = db_connect();
        $rep = db_query($db, "INSERT INTO sessions(user_id, session_id) VALUES(".$id.", '".session_id()."');");
        db_close($db);
    }
    
    global $membre_connecte;
    $membre_connecte = get_connected();
}

/* Désauthentification d'un utilisateur
 * @return: 1 true si la désauthentification a réussie, -1 sinon, 0 si pas utile
 */
function desauthentification() {
    // Suppression de la session dans la BDD et du cookie associé (si existant)
    if(isset($_COOKIE['user'])) {
        $db = db_connect();
        $rep = db_query($db, "DELETE FROM sessions WHERE session_id = '".$_COOKIE['user']."';");
        db_close($db);
        
        unset($_COOKIE['user']);
        setcookie('user', null, -1);
    }

    if(!isset($_SESSION["auth"])) // Si aucune session locale n'est ouverte, OK
        return 0;
    else { // Si une session locale est ouverte, on la supprime
        $_SESSION["auth"] = '';
        $_SESSION["user_id"] = '';
        return (session_destroy()) ? 1 : -1;
    }
}



/* Vérifie que le mot de passe correspond à l'utilisateur
 * @param: $id l'id de l'utilisateur
 * @param: $password le mot de passe de l'utilisateur (SHA1 non appliqué)
 * @return: true si le mot de passe est bon, false sinon
 */
function verif_mdp($id, $password) {
    $db = db_connect();
    $rep = db_query($db, "SELECT * FROM membres WHERE id = ".$id." AND password = '".sha1($password)."';");
    db_close($db);
    $fetch = db_fetch($rep);
    
    return (count($fetch) == 1);
}

/* Récupère les informations de l'utilisateur connecté, si il est connecté
 * @return: une tableau contenant les informations de l'utilisateur connecté, vide sinon
 */
function get_connected() {
    if(verif_connexion())
        return get_membre($_SESSION["user_id"]);
    return [];
}

/* Récupère une chaine de caractère et la rend plus lisible
 * @param: $str un string
 * @return: une chaîne plus sécurisée
 */
function secure_user_input($str) {
    $str = trim($str);
    $str = stripslashes($str);
    $str = htmlspecialchars($str);
    return $str;
}

/* Vérifie l'unicité d'un pseudo
 * @param: $pseudo le pseudo à tester
 * @return: 1 si on ne trouve pas de doublon dans la BDD, 0 sinon
 */
function is_unique_pseudo($pseudo){
    $db = db_connect();
    $rep = db_query($db, "SELECT * FROM membres WHERE LOWER(pseudo) = '" . strtolower($pseudo) . "';");
    db_close($db);
    
    return (db_count($rep) == 0) ? 1 : 0;
}

/* Vérifie l'unicité d'une adresse mail
 * @param: $email l'adresse mail à tester
 * @return: 1 si on ne trouve pas de doublon dans la BDD, 0 sinon
 */
function is_unique_mail($email) {
    $db = db_connect();
    $rep = db_query($db, "SELECT * FROM membres WHERE email = '" . $email . "';");
    db_close($db);
    
    return (db_count($rep) == 0) ? 1 : 0;
}

/* Indique si le joueur a donné une bonne réponse pour le contenu du lobby ou non
 * @param: $id_game l'id du lobby
 * @param: $id_membre l'id du membre
 * @return: true si a_donne_la_bonne_reponse = TRUE, false si = FALSE
 */
function a_donne_la_bonne_reponse($id_game, $id_membre) {
    $db = db_connect();
    $rep = db_query($db, "SELECT * FROM jouer WHERE id_game = ".$id_game." AND id_membre = ".$id_membre.";");
    db_close($db);
    $fetch = db_fetch($rep);

    return ($fetch[0]["a_donne_la_bonne_reponse"] == 't');
}

/* Indiquer dans la BDD que le joueur a donné la bonne réponse
 * @param: $id_game l'id du lobby
 * @param: $id_membre l'id du membre
 * @return: le nombre de lignes affectées par la commande UPDATE, 0 sinon
 */
function set_true_a_donne_la_bonne_reponse($id_game, $id_membre) {
    $db = db_connect();
    $rep = db_query($db, "UPDATE jouer SET a_donne_la_bonne_reponse = TRUE WHERE id_game = ".$id_game." AND id_membre = ".$id_membre.";");
    db_close($db);

    if($rep)
        return count(db_fetch($rep));
    return 0;
}

/* Modifie a_donne_la_bonne_reponse pour lui donner la valeur FALSE pour tous les joueurs
 * @param: $id_game l'id du lobby
 * @return: le nombre de lignes affectées par la commande UPDATE, 0 sinon
 */
function set_all_mauvaise_reponse($id_game) {
    $db = db_connect();
    $rep = db_query($db, "UPDATE jouer SET a_donne_la_bonne_reponse = FALSE WHERE id_game = ".$id_game.";");
    db_close($db);

    if($rep)
        return count(db_fetch($rep));
    return 0;
}

/* Change le contenu de se trouvant dans le lobby correspondant
 * @param: $lobby les informations sur le lobby
 */
function change_content($lobby) {
    $categorie = $lobby["categorie"];
    
    $db = db_connect();
    $rep1 = db_query($db, "SELECT DISTINCT contenu.id_contenu FROM contenu WHERE contenu.categorie_contenu = '" .$categorie. "';");
    $rep2 = db_query($db, "SELECT * FROM parties INNER JOIN contenu ON contenu.id_contenu = parties.id_contenu WHERE parties.id = ".$lobby["id"].";");
    db_close($db);

    $fichiers_selectionnes = db_fetch($rep1);
    $fetch = db_fetch($rep2);

    if(count($fetch) == 1) { // si on a bien qu'un seul résultat
        $id_actuel = $fetch[0]["id_contenu"];

        do {
            $position_nouveau = rand(1, count($fichiers_selectionnes)) - 1;
            $id_nouveau = $fichiers_selectionnes[$position_nouveau][0];
        } while ($id_nouveau == $id_actuel); // On s'assure de ne pas avoir le même contenu
    }
    
    $db = db_connect();
    $rep = db_query($db, "UPDATE parties SET id_contenu = ".$id_nouveau." WHERE id = ".$lobby["id"].";");
    db_close($db);
}

/* Permet de vérifier qu'un tableau d'erreurs ne contient aucune erreur
 * @param: $errors un tableau d'erreurs
 * @return: 0 si il n'y a pas d'erreur, 1 si il y a une erreur
 */
function has_error($errors) {
    $retour = 0;
    foreach($errors as $error)
        $retour += $error;
    return $retour;
}
?>