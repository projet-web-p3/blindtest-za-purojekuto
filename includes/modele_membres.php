<?php
/* Récupère les informations de l'utilisateur
 * @param: $id l'id de l'utilisateur
 * @param: $pseudo le pseudo de l'utilisateur (facultatif)
 * @return: un tableau contenant les informations de l'utilisateur, vide sinon
 */
function get_membre($id, $pseudo = "") {
    $id = intval($id); // Permet d'éviter d'avoir des chaînes à la place d'un entier
    
    $db = db_connect();
    $rep = db_query($db, "SELECT * FROM membres WHERE id = ".$id." OR LOWER(pseudo) = '".strtolower($pseudo)."';");
    db_close($db);
    $fetch = db_fetch($rep);
    
    if(count($fetch) == 1)
        return $fetch[0];
    return [];
}

/* Récupère la liste des membres, en considérant les restrictions en argument
 * @param: $pseudo le morceau que le pseudo doit contenir
 * @return: un tableau contenant les informations des membres, vide sinon
 */
function get_liste_membres($pseudo) {
    $db = db_connect();
    $rep = db_query($db, "SELECT * FROM membres WHERE LOWER(pseudo) LIKE '". strtolower($pseudo) ."%' ORDER BY id DESC;");
    db_close($db);
    $fetch = db_fetch($rep);
    
    if(count($fetch) > 0)
        return $fetch;
    return [];
}

/* Ajout d'un membre à la base de données
 * @param: $pseudo le pseudo du nouveau membre
 * @param: $password le mot de passe du nouveau membre
 * @param: $email le mail du nouveau membre
 */
function ajout_membre($pseudo, $password, $email) {
    $db = db_connect();
    $rep = db_query($db, "INSERT INTO membres VALUES (DEFAULT, current_timestamp, '$pseudo', '$password', '$email', 1)");
    db_close($db);
}

/* Mise à jour d'un membre dans la base de données
 * @param: $id l'id du membre mis à jour
 * @param: $pseudo le pseudo du membre mis à jour
 * @param: $password le mot de passe du membre mis à jour
 * @param: $email le mail du membre mis à jour
 * @return: un tableau associatif de trois éléments
     - premier élément : 1 si on change de pseudo, 0 sinon
     - second élément : 1 si on change de mot de passe, 0 sinon
     - troisième élément : 1 si on change de mail, 0 sinon
 */
function update_membre($id, $pseudo, $password, $email) {
    $retour["pseudo"] = 0;
    $retour["password"] = 0;
    $retour["email"] = 0;
    
    $db = db_connect();
    if($pseudo) {
        $rep = db_query($db, "UPDATE membres SET pseudo = '".$pseudo."' WHERE ID = ".$id.";");
        $retour["pseudo"] = 1;
    }
    if($password) {
        $rep = db_query($db, "UPDATE membres SET password = '".sha1($password)."' WHERE ID = ".$id.";");
        // Comme l'utilisateur change de pseudo, on le delog de toutes ses sessions
        $rep = db_query($db, "DELETE FROM sessions WHERE user_id = ".$id.";");
        $retour["password"] = 1;
    }
    if($email) {
        $rep = db_query($db, "UPDATE membres SET email = '".$email."' WHERE ID = ".$id.";");
        $retour["email"] = 1;
    }
    db_close($db);
    
    return $retour;
}


/*----------------------------------------------
-------- CHANGEMENT DU NIVEAU DU MEMBRE -------- 
----------------------------------------------*/


/* Mets à jour la BDD en changeant le niveau hiérarchique d'un membre
 @param: $membre le tableau des informations du membre
 @param: $level le nouveau niveau hiérarchique du membre
 @return: le nombre de lignes affectées par la requête UPDATE
 */
function set_membre_level($membre, $level = 1) {
    $db = db_connect();
    $rep = db_query($db, "UPDATE membres SET hierarchie = ".$level." WHERE id = ".$membre["id"].";");
    db_close($db);

    if($rep)
        return count(db_fetch($rep));
    return 1;
}

/* Mets à jour la BDD en rendant un membre banni */
function set_membre_banni($membre) {
    return set_membre_level($membre, 0);
}

/* Mets à jour la BDD en rendant un membre simple membre */
function set_membre_simple($membre) {
    return set_membre_level($membre, 1);
}

/* Mets à jour la BDD en rendant un membre administrateur */
function set_membre_admin($membre) {
    return set_membre_level($membre, 2);
}


/*----------------------------------------------
---- FONCTIONS DE TESTS DU NIVEAU DU MEMBRE ----
----------------------------------------------*/


/* Test du niveau "Banni" du membre
 @param: $membre le tableau des informations du membre
 @return: true si le membre est banni, false sinon
 */
function is_membre_banni($membre) {
    return $membre["hierarchie"] == 0;
}

/* Test du niveau "Simple membre" du membre
 @param: $membre le tableau des informations du membre
 @return: true si le membre est simple membre, false sinon
 */
function is_membre_simple($membre) {
    return $membre["hierarchie"] == 1;
}

/* Test du niveau "Administrateur" du membre
 @param: $membre le tableau des informations du membre
 @return: true si le membre est administrateur, false sinon
 */
function is_membre_admin($membre) {
    return $membre["hierarchie"] == 2;
}
?>