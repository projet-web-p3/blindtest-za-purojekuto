<?php
include_once("includes/modele.php");

include_once("vue_formulaires.php");
include_once("vue_membres.php");
include_once("vue_lobbys.php");

/* En-tête de la page
 * @param: $titre le titre de la page courante
 */
function enTete($titre) {
    echo '
    <!DOCTYPE html>
    <html>
        <head>
            <meta charset="utf-8" />
            <title>'.$titre.'</title>
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
            <link rel="icon" href="resources/favicon.ico" type="image/x-icon" />
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <meta name="theme-color" content="#3F51B5">
            <style type="text/css">
                body { background: #eeeeee !important; } /* fond decran de toutes les pages */
                .table, th, td { border: 1px solid #b3b3b3 !important; vertical-align: middle !important;  } /* bordures des tableaux */
            </style>
        </head>
        <body>';
            
    navigation();
            
    echo '
    <div class="container">
        <header>
            <h1>'.$titre.'</h1>
        </header>';
}

/* Zone de navigation sur le site */
function navigation() {
    global $membre_connecte; // Informations sur le membre connecté (vide si non connecté)
    $page = basename($_SERVER['PHP_SELF']); // Page actuelle
    
    echo '
    <nav class="navbar navbar-inverse navbar-static-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">Quiz : Za Purojekuto</a>
            </div>
            
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">';

                if($page == "index.php") // Si la page actuelle est l'index
                    echo '<li class="active"><a href="index.php">Accueil</a></li>';
                else
                    echo '<li><a href="index.php">Accueil</a></li>';

                if(verif_connexion()) {
                    if($page == "profil.php") // Si la page actuelle est un profil
                        echo '<li class="active"><a href="profil.php">Profil</a></li>';
                    else
                        echo '<li><a href="profil.php">Profil</a></li>';
                }

                if($page == "liste_membres.php") // Si la page actuelle est la liste des membres
                    echo '<li class="active"><a href="liste_membres.php">Liste des membres</a></li>';
                else
                    echo '<li><a href="liste_membres.php">Liste des membres</a></li>';

                if($page == "lobby.php") // Si la page actuelle est la liste des lobbys ou un lobby
                    echo '<li class="active"><a href="lobby.php">Lobbys</a></li>';
                else
                    echo '<li><a href="lobby.php">Lobbys</a></li>';

            echo '
                </ul>
                <ul class="nav navbar-nav navbar-right">';
                
                if(!verif_connexion())
                    echo '<li><a href="inscription.php"><span class="glyphicon glyphicon-user"></span> Inscription</a></li>
                    <li><a href="connexion.php"><span class="glyphicon glyphicon-log-in"></span> Connexion</a></li>';
                else
                    echo '<li><a href="deconnexion.php"><span class="glyphicon glyphicon-log-out"></span> Déconnexion</a></li>';
                    
            echo '</ul>';

            if(verif_connexion())
                echo '<p class="navbar-text navbar-right">Bienvenue, '.$membre_connecte["pseudo"].'</p>';
                
            echo '
            </div>
        </div>
    </nav>';
}

/* Pied de la page */
function pied() {
    echo '</div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>';
    // le javascript bootstrap ne sert que pour le bouton navigation en responsive design
        
    $page = basename($_SERVER['PHP_SELF']); // Page actuelle
    if($page == "lobby.php")
        echo '<script src="lobby.js" type="text/javascript"></script>';
        
    echo '</body>
    </html>';
}


/*----------------------------------------------
------------- AFFICHAGES BASIQUES --------------
----------------------------------------------*/


/* Affichage basique d'une chaîne de caractères */
function affiche($str) { echo $str; }

/* Affichage d'une information
 * @param: $str la chaîne de caractères à afficher
 */
function affiche_info($str) {
    echo '
    <div class="alert alert-info" role="alert">
        <span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
        '.$str.'
    </div>';
}

/* Affichage d'une erreur
 * @param: $str la chaîne de caractères à afficher
 */
function affiche_erreur($str) {
    echo '
    <div class="alert alert-danger" role="alert">
        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
        '.$str.'
    </div>';
}

/* Affichage d'un succès
 * @param: $str la chaîne de caractères à afficher
 */
function affiche_succes($str) {
    echo
    '<div class="alert alert-success" role="alert">
        <span class="glyphicon glyphicon glyphicon-ok" aria-hidden="true"></span>
        '.$str.'
    </div>';
}

/* Affichage d'un warning
 * @param: $str la chaîne de caractères à afficher
 */
function affiche_warning($str) {
    echo
    '<div class="alert alert-warning" role="alert">
        <span class="glyphicon glyphicon-warning-sign" aria-hidden="true"></span>
        '.$str.'
    </div>';
}


/*----------------------------------------------
------------ MORCEAUX DE FORMULAIRE ------------
----------------------------------------------*/


/* Morceau de formulaire pour l'envoie du formulaire
 * @param: $value le libellé du bouton d'envoi
 * @param: $id_button un ID pour le bouton (facultatif)
 */
function form_group_submit($value, $id_button = "") {
    echo '
    <div class="form-group"> 
        <div class="col-sm-offset-3 col-sm-9">
            <button type="submit" class="btn btn-default" id="'.$id_button.'">'.$value.'</button>
        </div>
    </div>';
}

/* Morceau de formulaire
 * @param: $type le type d'input pour ce morceau de formulaire
 * @param: $legend la légende de ce morceau de formulaire
 * @param: $name le nom de l'input de ce morceau de formulaire
 * @param: $placeholder le placeholder pour l'input de ce morceau de formulaire
 * @param: $value la valeur par défaut de l'input de ce morceau de formulaire
 * @param: $required qui vaut "" ou "required"
 */
function form_group($type, $legend, $name, $placeholder, $value, $required = "") {
    echo '
    <div class="form-group">
        <label for="'.$name.'" class="control-label col-sm-3">'.$legend.'</label>
        <div class="col-sm-9">
            <input type="'.$type.'" name="'.$name.'" id="'.$name.'" placeholder="'.$placeholder.'" value="'.$value.'"  class="form-control" '.$required.' />
        </div>
    </div>';
}

/* Morceau de formulaire contenant un warning
 * @param: $type le type d'input pour ce morceau de formulaire
 * @param: $legend la légende de ce morceau de formulaire
 * @param: $name le nom de l'input de ce morceau de formulaire
 * @param: $placeholder le placeholder pour l'input de ce morceau de formulaire
 * @param: $value la valeur par défaut de l'input de ce morceau de formulaire
 * @param: $msg_warning le message de warning à afficher
 * @param: $required qui vaut "" ou "required"
 */
function form_group_warning($type, $legend, $name, $placeholder, $value, $msg_warning, $required = "") {
    echo '
    <div class="form-group has-warning has-feedback">
        <label for="'.$name.'" class="control-label col-sm-3">'.$legend.'</label>
        <div class="col-sm-9">
            <input type="'.$type.'" name="'.$name.'" id="'.$name.'" placeholder="'.$placeholder.'" value="'.$value.'" class="form-control" '.$required.' />
            <span class="glyphicon glyphicon-warning-sign form-control-feedback"></span>
            <span class="help-block">'.$msg_warning.'</span>
        </div>
    </div>';
}


/* Morceau de formulaire contenant une erreur
 * @param: $type le type d'input pour ce morceau de formulaire
 * @param: $legend la légende de ce morceau de formulaire
 * @param: $name le nom de l'input de ce morceau de formulaire
 * @param: $placeholder le placeholder pour l'input de ce morceau de formulaire
 * @param: $value la valeur par défaut de l'input de ce morceau de formulaire
 * @param: $msg_error le message d'erreur à afficher
 */
function form_group_error($type, $legend, $name, $placeholder, $value, $msg_error, $required = "") {
    echo '
    <div class="form-group has-error has-feedback">
        <label for="'.$name.'" class="control-label col-sm-3">'.$legend.'</label>
        <div class="col-sm-9">
            <input type="'.$type.'" name="'.$name.'" id="'.$name.'" placeholder="'.$placeholder.'" value="'.$value.'" class="form-control" '.$required.' />
            <span class="glyphicon glyphicon-remove form-control-feedback"></span>
            <span class="help-block">'.$msg_error.'</span>
        </div>
    </div>';
}

/* Mets en gras la partie correspondante du mot
 * @param: $str la chaîne ç traiter
 * @param: $highlight la chaîne à mettre en gras dans $str
 */
function highlight($str, $highlight) {
    if($highlight != "")
        return preg_replace("/".$highlight."/i", '<b>$0</b>', $str);
    else
        return $str;
}
?>