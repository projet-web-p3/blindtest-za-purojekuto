<?php
/* Affichage du profil d'un utilisateur
 * @param: $membre les informations de l'utilisateur dont on veut afficher le profil
 */
function vue_profil($membre) {
    echo '
    <table class="table table-bordered">
        <thead>
            <tr>
                <th></th>
                <th>Pseudo</th>
                <th>Email</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td style="padding: 0; width: 80px;">
                    <img src="https://fr.gravatar.com/avatar/'.md5($membre["email"]).'" alt="avatar" style="width: 80px;" />
                </td>
                <td>'.$membre["pseudo"].'</td>
                <td>'.$membre["email"].'</td>
            </tr>
        </tbody>
    </table>
    ';
}

/* Affichage de la liste des membres
 * @param: $membres les informations des membres
 * @param: $pseudo un filtre de recherche sur les pseudos
 * @param: $hierarchies l'ensemble des niveaux hiérarchiques possibles pour un membre
 */
function vue_liste_membres($membres, $pseudo, $hierarchies) {
    global $membre_connecte;

    echo '
    <table class="table table-bordered table-striped table-responsive">
        <thead>
            <tr>
                <th></th>
                <th>ID</th>
                <th>Pseudo</th>
                <th>Mail</th>
                <th>Niveau hiérarchique</th>
                <th>Inscription</th>';
            
            // Si on est admin, on affiche les boutons de gestion du membre
            if(verif_connexion() && is_membre_admin($membre_connecte))
                    echo '<th>Gestion</th>';
                
            echo'
            </tr>
        </thead>
        <tbody>';
    if(count($membres) > 0) {
        foreach($membres as $membre) {
            if(is_membre_banni($membre))
                echo '<tr class="danger">';
            elseif(is_membre_admin($membre))
                echo '<tr class="success">';
            else
                echo '<tr>';
            echo '
                <td style="padding: 0; width: 40px;">
                    <img src="https://fr.gravatar.com/avatar/'.md5($membre["email"]).'" alt="avatar" style="width: 40px;" />
                </td>
                <td align="center" style="width: 40px;">' .$membre["id"]. '</td>
                <td><a href="profil.php?id=' .$membre["id"]. '">' .highlight($membre["pseudo"], $pseudo). '</a></td>
                <td>' .$membre["email"]. '</td>
                <td>' .$hierarchies[$membre["hierarchie"]]. '</td>
                <td>' .date('d/m/Y à H:i:s', strtotime($membre["inscription"])). '</td>';
                
                // Si on est admin, on affiche les boutons de gestion du membre
                if(verif_connexion() && is_membre_admin($membre_connecte)) {
                    echo '<td>';
                    // Si le membre n'est pas banni et n'est pas admin, on peut le bannir
                    if(!is_membre_banni($membre) && !is_membre_admin($membre)) {
                        echo '
                        <a href="gestion_membre.php?id=' .$membre["id"]. '&ban" class="btn btn-danger btn-xs" role="button">
                            Bannir <span class="glyphicon glyphicon-fire" aria-hidden="true"></span> 
                        </a>';
                    }
                    // Si le membre est banni, on peut le pardonner
                    if(is_membre_banni($membre)) {
                        echo '
                        <a href="gestion_membre.php?id=' .$membre["id"]. '&unban" class="btn btn-success btn-xs" role="button">
                            Pardonner <span class="glyphicon glyphicon-tint" aria-hidden="true"></span> 
                        </a>';
                    }
                    echo '</td>';
                }
            echo '</tr>';
        }
    }
    else
        echo '<tr><td colspan="25">Aucun membre correspondant au filtre n\'a été trouvé.</td></tr>';

    echo '
        </tbody>
    </table>';
}
?>