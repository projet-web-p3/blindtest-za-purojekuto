<?php
/* Récupère les informations du joueur
 * @param: $id_membre l'id du membre
 * @param: $id_game l'id du lobby
 * @return: un tableau contenant les informations du joueur, vide sinon
 */
function get_joueur($id_membre, $id_game) {
    $id_membre = intval($id_membre); // Permet d'éviter d'avoir des chaînes à la place d'un entier
    $id_game = intval($id_game); // Permet d'éviter d'avoir des chaînes à la place d'un entier
    
    $db = db_connect();
    $rep = db_query($db, "SELECT * FROM jouer WHERE id_membre = ".$id_membre." AND id_game = ".$id_game.";");
    db_close($db);
    $fetch = db_fetch($rep);
    
    if(count($fetch) == 1)
        return $fetch[0];
    return [];
}

/* Ajout d'un joueur dans un lobby
 * @param: $id_game l'id du lobby
 * @param: $id_membre l'id du membre
 * @return: le nombre d'opérations effectuées
 */
function ajout_joueur($id_game, $id_membre) {
    $db = db_connect();
    $rep = db_query($db, "INSERT INTO jouer VALUES(".$id_membre.", ".$id_game.", FALSE, 0, FALSE);");
    db_close($db);

    if($rep)
        return count(db_fetch($rep));
    else
        return 0;
}

/* Suppression d'un joueur dans un lobby
 * @param: $id_game l'id du lobby
 * @param: $id_membre l'id du membre
 * @return: le nombre d'opérations effectuées
 */
function remove_joueur($id_game, $id_membre) {
    $db = db_connect();
    $rep = db_query($db, "DELETE FROM jouer WHERE id_game = ".$id_game." AND id_membre = ".$id_membre.";");
    db_close($db);

    if($rep)
        return count(db_fetch($rep));
    else
        return 0;
}

/* Récupération de la liste des joueurs
 * @param: $id_game l'id du lobby
 * @return: un tableau contenant la liste des joueurs du lobby
 */
function get_liste_joueurs($id_game) {
    $db = db_connect();
    $rep = db_query($db, "SELECT * FROM jouer INNER JOIN membres ON membres.id = jouer.id_membre WHERE id_game = ".$id_game." ORDER BY score DESC, pseudo ASC;");
    db_close($db);
    $fetch = db_fetch($rep);
    
    if(count($fetch) > 0)
        return $fetch;
    return [];
}

/* Test d'appartenance d'un joueur à un lobby
 * @param: $id_game l'id du lobby
 * @param: $id_membre l'id du membre
 * @return: le nombre de lignes obtenues
 */
function is_joueur_in_lobby($id_game, $id_membre) {
    $db = db_connect();
    $rep = db_query($db, "SELECT * FROM jouer WHERE id_game = ".$id_game." AND id_membre = ".$id_membre.";");
    db_close($db);

    if($rep)
        return count(db_fetch($rep));
    return 0;
}

/* Test de l'état "Prêt" du joueur
 @param: $joueur le tableau des informations du joueur
 @return: true si le joueur est prêt, false sinon
 */
function is_joueur_ready($joueur) {
    return $joueur["ready"] == 't';
}

/* Ajout de score à un joueur
 * @param: $joueur les informations du joueur (table jouer)
 * @param: $score_boost le score à ajouter
 */
function add_score($joueur, $score_boost) {
    $db = db_connect();
    $rep = db_query($db, 'UPDATE jouer SET score = score + '.$score_boost.' WHERE id_membre = '.$joueur["id_membre"].' AND id_game = '.$joueur["id_game"].';');
    db_close($db);
    
    if($rep)
        return count(db_fetch($rep));
    return 1;
}
?>