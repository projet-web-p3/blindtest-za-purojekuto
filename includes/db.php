<?php
include("includes/config.php");

/* Connexion à la base de données
 * @return: un objet PDO associé à la connexion
 */
function db_connect() {
    global $HOST, $USER, $BASE, $PASS; // Variables d'accès à la base de données
    
    try {
        $db = new PDO('pgsql:host='.$HOST.';dbname='.$BASE, $USER, $PASS);
    } catch(PDOException $e) {
        print "PDO Error: " . $e->getMessage() . "<br/>";
        die();
    }
    
    return $db;
}

/* Requête sur la base de données
 * @param: $db l'objet PDO associé à la connexion
 * @param: $query la requête SQL à exécuter
 * @return: l'objet PDOStatement associé (tableau des lignes résultat)
 */
function db_query($db, $query) {
    return $db->query($query);
}

/* Place les résultats de la requête dans un tableau
 * @param: $rep l'objet PDOStatement issu d'une requête
 * @return: un tableau contenant les lignes résultant de la requête
 */
function db_fetch($rep) {
    return $rep->fetchAll();
}

/* Nombre de lignes d'une requête DELETE, INSERT, UPDATE
 * @param: $rep l'objet PDOStatement issu d'une requête
 * @return: le nombre de lignes d'une requête SELECT
 */
function db_count($rep) {
    return $rep->rowCount();
}

/* Déconnexion de la base de données : suppression de l'objet
 * @param: $db l'objet PDO associé à la connexion
 */
function db_close(&$db) {
    $db = null;
}