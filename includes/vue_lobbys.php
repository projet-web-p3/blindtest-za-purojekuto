<?php
/* Affichage de la liste des lobbys avec les actions qui lui sont liées
 * @param: $lobbys la liste des lobbys
 * @param: $etats la liste des états possibles pour un lobby
 */
function vue_liste_lobbys($lobbys, $etats) {
    global $membre_connecte; // Informations sur le membre connecté (vide si non connecté)
    
    echo '
    <table class="table table-bordered table-striped table-condensed table-responsive">
        <thead>
            <tr>
                <th>ID</th>
                <th>Date</th>
                <th>Créateur</th>
                <th>Catégorie</th>
                <th>Etat</th>';
                if(verif_connexion())
                    echo '<th></th>'; //la colonne pour rejoindre/quitter/supprimer une partie
                
            echo '
            </tr>
        </thead>
        <tbody>';
      
    if(count($lobbys) > 0)
        foreach($lobbys as $lobby) {
            if(verif_connexion()) { // si l'utilisateur est connecté, on rajoute un peu de couleur dans certains cas
                
                // Ajout d'un fond rouge si la personne est connectée et a créé cette partie ET la partie est "en attente" ou "en cours"
                if( is_creator_of_lobby($lobby, $membre_connecte["id"]) && (is_lobby_attente($lobby) || is_lobby_encours($lobby)) ) 
                    echo '<tr class="danger">';
                    
                // Ajout d'un fond bleu si la personne est connectée et a créé cette partie
                elseif(is_creator_of_lobby($lobby, $membre_connecte["id"]))
                    echo '<tr class="warning">';
            }
            else
                echo '<tr>';
            
            echo '
                <td>' .$lobby["id"]. '</td>
                <td>' .date('d/m/Y à H:i:s', strtotime($lobby["creation"])). '</td>
                <td>' .get_membre($lobby["id_createur"])["pseudo"]. '</td>
                <td>' .$lobby["categorie"]. '</td>
                <td>' .$etats[$lobby["etat"]]. '</td>';
                
                if(verif_connexion()) {
                    echo '<td>';// on est dans la colonne pour rejoindre/quitter/supprimer une partie
    
                    // si le lobby est "en attente" ou "en cours"
                    if( is_lobby_attente($lobby) || is_lobby_encours($lobby) ) {
                        if(is_joueur_in_lobby($lobby["id"], $membre_connecte["id"])) { // si le joueur est dans le lobby
                            echo '
                            <a href="lobby.php?id=' .$lobby["id"]. '" class="btn btn-success btn-xs" role="button">
                                Continuer <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span> 
                            </a>'; // il peut continuer ...
                            echo ' 
                            <a href="lobby.php?id=' .$lobby["id"]. '&exit" class="btn btn-warning btn-xs" role="button">
                                Quitter <span class="	glyphicon glyphicon-remove-circle" aria-hidden="true"></span> 
                            </a>'; // ... ou quitter
                        }
                        else
                            echo '
                            <a href="lobby.php?id=' .$lobby["id"]. '" class="btn btn-primary btn-xs" role="button">
                                Rejoindre <span class="glyphicon glyphicon-hand-right" aria-hidden="true"></span> 
                            </a>'; // s'il n'est pas dans le lobby, il peut le rejoindre
                    }
                    
                    // si le lobby est "fini"
                    if( is_lobby_fini($lobby) )
                        echo '
                        <a href="lobby.php?id=' .$lobby["id"]. '" class="btn btn-default btn-xs" role="button">
                            Voir <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> 
                        </a>';
    
                    // Si la partie est "en attente" et que l'utilisateur est le créateur de la partie OU est un admin, on lui propose de la supprimer
                    if( is_lobby_attente($lobby) && (is_creator_of_lobby($lobby, $membre_connecte["id"]) || is_membre_admin($membre_connecte)) )
                        echo ' 
                        <a href="suppression_lobby.php?id=' .$lobby["id"]. '" class="btn btn-danger btn-xs" role="button">
                            Supprimer <span class="glyphicon glyphicon-trash" aria-hidden="true"></span> 
                        </a>';
                        
                    echo '</td>';
                }
            echo '</tr>';
        }
    else
        echo '<tr><td colspan="25">Aucune partie disponible.</td></tr>';
        
    echo '
        </tbody>
    </table>';
}

/* Affichage de la liste des joueurs dans un lobby
 * @param: $joueurs le résultat de la requête donnant les joueurs du lobby, et leurs informations
 * @param: $lobby toutes les informations du lobby en question
 * @param: $connecte les informations du membre actuellement connecté
 */
function vue_liste_joueurs($joueurs, $lobby, $connecte, $jsok = "") {
    echo "<h3>Joueurs</h3>";
    echo '
    <table class="table table-bordered table-striped table-condensed table-responsive">
        <thead>
            <tr>
                <th></th>
                <th>Pseudo</th>
                <th>Score</th>';
                if(is_lobby_attente($lobby)) // si la partie est "en attente"
                    echo '<th width="300px">Status</th>';
            echo '
            </tr>
        </thead>
        <tbody>';

    if(count($joueurs) > 0) {
        foreach($joueurs as $joueur) {
            if(is_creator_of_lobby($lobby, $joueur["id"]))
                echo '<tr class="warning">';
            else
                echo '<tr>';
            echo '
            <td style="padding: 0; width: 40px;">
                <img src="https://fr.gravatar.com/avatar/'.md5($joueur["email"]).'" alt="avatar" style="width: 40px;" />
            </td>
            <td>' .$joueur["pseudo"]. '</td>
            <td>' .$joueur["score"]. '</td>';
            
            if(is_lobby_attente($lobby)) { // Si la partie est "en attente" 
                echo '<td>';
                
                if($joueur["id_membre"] == $connecte["id"]) { // Si c'est la ligne du joueur, on lui propose de changer prêt <-> pas prêt
                
                    if($jsok) // Si JavaScript est actif
                        echo '<form id="change_status_form">';
                    else // Si JavaScript n'est pas actif
                        echo '<form id="change_status_form" action="change_ready.php" method="GET">';

                    if(is_joueur_ready($joueur)) { // si le joueur est prêt
                        $text = "Prêt";
                        $class = "btn-success";
                    }
                    else { // si le joueur n'est pas prêt
                        $text = "Indisponible";
                        $class = "btn-danger";
                    }
                        
                    echo '
                        <input type="hidden" name="id_lobby" id="id_lobby" value="'.$lobby["id"].'" />
                        <input type="hidden" name="id_user" id="id_user" value="'.$connecte["id"].'" />';
                        
                    if($jsok) // Si JavaScript est actif
                        echo '<button type="button" id="btn_change_status" class="btn '.$class.' btn-xs" onclick="change_ready()">'.$text.'</button>';
                    else
                        echo '<button type="submit" id="btn_change_status" class="btn '.$class.' btn-xs" onclick="change_ready()">'.$text.'</button>';
                        
                    echo '
                        <div id="spinner" style="display:none;"><img src="resources/spinner.gif" alt="spinner" style="width:14px; height:14px;" /></div>
                    </form>';
                    
                }

                else { // Si c'est la ligne d'un autre joueur que l'utilisateur connecté
                    if(is_joueur_ready($joueur)) // si le joueur est prêt
                        echo '<button type="button" class="btn btn-success btn-xs disabled">Prêt</button>';
                    else // si le joueur n'est pas prêt
                        echo '<button type="button" class="btn btn-danger btn-xs disabled">Indisponible</button>';
                }

                echo '</td>';
            }
            echo '</tr>';
        }
    }
    else
        echo '<tr><td colspan="25">Ce lobby est vide.</td></tr>';

    echo '
        </tbody>
    </table>';
}

/* Affichage du contenu pour un certain lobby
 * @param: $lobby toutes les informations des tables lobby INNER JOIN contenu
 */
function afficher_contenu($lobby) {
    if(is_lobby_encours($lobby)) { // Si la partie est en cours
        if($lobby["type_contenu"] == "image") // si le contenu est une image
            echo '<img src="'.$lobby["url"].'">';
        elseif($lobby["type_contenu"] == "musique") { // si le contenu est de l'audio
            echo '
                <audio id="myAudio" autoplay="" loop="">
                    <source id="idSource" src="'.$lobby["url"].'" type="audio/mp3">
                    Problème pour jouer la musique.
                </audio>
                <div class="btn btn-info" style="pointer-events: none;">Musique en cours !</div>';
        }
        else
            echo "Aucun contenu.";
    }
    elseif(is_lobby_attente($lobby)) // Si la partie est en attente
        affiche_info("La partie est en attente.");
    elseif(is_lobby_fini($lobby)) // Si la partie est finie
        affiche_info("La partie est finie.");
}
?>