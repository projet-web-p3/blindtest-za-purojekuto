<?php
/* Formulaire de connexion
 * @param: $pseudo un pseudo pour pré-remplir le formulaire de connexion
 */
function vue_connexion($errors, $pseudo = "") {
    echo '
    <form action="connexion.php" method="POST" class="form-horizontal">
        <input type="hidden" name="connexion_form" value="connexion_form" />';

    // Champ pseudo
    if($errors["empty_pseudo"])
        form_group_error("text", "Pseudo", "pseudo", "Entrez votre pseudo", $pseudo, "Veuillez entrer votre pseudo", "required");
    elseif($errors["failure_pseudo"])
        form_group_error("text", "Pseudo", "pseudo", "Entrez votre pseudo", $pseudo, "Ce pseudo n'existe pas", "required");
    else
        form_group("text", "Pseudo", "pseudo", "Entrez votre pseudo", $pseudo, "required");
    
    // Champ mot de passe
    if($errors["empty_password"])
        form_group_error("password", "Mot de passe", "password", "Entrez votre mot de passe", "", "Veuillez entrer votre mot de passe", "required");
    elseif($errors["failure_pwd"])
        form_group_error("password", "Mot de passe", "password", "Entrez votre mot de passe", "", "Le mot de passe entré ne correspond pas à l'utilisateur", "required");
    else
        form_group("password", "Mot de passe", "password", "Entrez votre mot de passe", "", "required");

        echo '
        <div class="form-group"> 
            <div class="col-sm-offset-3 col-sm-9">
                <div class="checkbox">
                    <label><input type="checkbox" name="remember"> Rester connecté</label>
                </div>
            </div>
        </div>';

    form_group_submit("Connexion");

    echo '</form>';
}

/* Formulaire d'édition du profil 
 * @param: $membre les informations de l'utilisateur à éditer
 */
function vue_edition_profil($membre, $errors) {
    echo '
    <form action="profil.php?id='.$membre["id"].'" method="POST" class="form-horizontal">
        <input type="hidden" name="edit_profil_form" value="edit_profil_form" />';
    
    // Champ nouveau pseudo
    if($errors["failure_pseudo"])
        form_group_error("text", "Nouveau pseudo", "pseudo", $membre["pseudo"], "", "Le nouveau pseudo est déjà utilisé", "required");
    else
        form_group("text", "Nouveau pseudo", "pseudo", $membre["pseudo"], "", "");
    
    // Champ nouveau mail
    if($errors["failure_email"])
        form_group_error("email", "Nouveau mail", "email", $membre["email"], "", "La nouvelle adresse mail n'est pas valide", "required");
    elseif($errors["failure_email_exists"])
        form_group_error("email", "Nouveau mail", "email", $membre["email"], "", "La nouvelle adresse mail est déjà utilisée", "required");
    else
        form_group("email", "Nouveau mail", "email", $membre["email"], "", "");
    
    // Champ nouveau mot de passe
    form_group("password", "Nouveau mot de passe", "newPassword", "Nouveau mot de passe", "", "");
    
    // Champ confirmation du nouveau mot de passe
    if($errors["failure_conf_pwd"])
        form_group_error("password", "Confirmation", "newPasswordConfirm", "Confirmation du nouveau mot de passe", "", "Les deux mots de passe rentrés ne concordent pas", "required");
    else
        form_group("password", "Confirmation", "newPasswordConfirm", "Confirmation du nouveau mot de passe", "", "");
    
    // Champ ancien mot de passe
    if($errors["wrong_pwd"])
        form_group_error("password", "Mot de passe actuel", "password", "Entrez votre mot de passe (Obligatoire)", "", "L'ancien mot de passe est erronné", "required");
    else
        form_group_warning("password", "Mot de passe actuel", "password", "Entrez votre mot de passe", "", "Ce champ est obligatoire", "required");

    form_group_submit("Modifier le profil");

    echo '</form>';
}

/* Formulaire d'inscription
 * @param: $errors un tableau stockant les erreurs du formulaire
 * @param: $pseudo un pseudo pour pré-remplir le formulaire d'inscription
 * @param: $email un mail pour pré-remplir le formulaire d'inscription
 */
function vue_inscription($errors, $pseudo = "", $email = "") {
    echo '
    <form action="inscription.php" method="POST" class="form-horizontal">
        <input type="hidden" name="inscription_form" value="inscription_form" />';

    // Champ pseudo
    if($errors["empty_pseudo"])
        form_group_error("text", "Pseudo", "pseudo", "Entrez votre pseudo", $pseudo, "Le pseudo ne peut pas être vide", "required");
    elseif($errors["failure_pseudo"])
        form_group_error("text", "Pseudo", "pseudo", "Entrez votre pseudo", $pseudo, "Le pseudo demandé est déjà existant", "required");
    else
        form_group("text", "Pseudo", "pseudo", "Entrez votre pseudo", $pseudo, "required");

    // Champ email
    if($errors["empty_email"])
        form_group_error("email", "Email", "email", "Entrez votre mail", $email, "Le mail ne peut pas être vide", "required");
    elseif($errors["failure_email"])
        form_group_error("email", "Email", "email", "Entrez votre mail", $email, "L'adresse mail n'est pas valide", "required");
    elseif($errors["failure_email_exists"])
        form_group_error("email", "Email", "email", "Entrez votre mail", $email, "L'adresse mail est déjà associée à un autre compte", "required");
    else
        form_group("email", "Email", "email", "Entrez votre mail", $email, "required");
 
    // Champ mot de passe
    if($errors["empty_password"])
        form_group_error("password", "Mot de passe", "password", "Entrez votre mot de passe", "", "Veuillez rentrer un mot de passe", "required");
    else
        form_group("password", "Mot de passe", "password", "Entrez votre mot de passe", "", "required");
    
    // Champ confirmation du mot de passe
    if($errors["empty_conf_password"])
        form_group_error("password", "Confirmation", "conf_password", "Confirmez votre mot de passe", "", "Veuillez confirmer votre mot de passe", "required");
    elseif($errors["failure_pwd"])
        form_group_error("password", "Confirmation", "conf_password", "Confirmez votre mot de passe", "", "Les deux mots de passe rentrés ne concordent pas", "required");
    else
        form_group("password", "Confirmation", "conf_password", "Confirmez votre mot de passe", "", "required");


    form_group_submit("Inscription");

    echo '</form>';
}

/* Formulaire de recherche des membres
 * @param: $pseudo le pseudo recherché auparavant
 */
function vue_recherche_membre($pseudo) {
    echo '
    <form action="liste_membres.php" method="GET" class="form-horizontal">
        <div class="input-group input-group-lg">
            <span class="input-group-addon">@</span>
            <input type="text" name="pseudo" class="form-control" placeholder="Pseudo du membre recherché" value="'.$pseudo.'">
            <span class="input-group-btn">
                <button class="btn btn-default" type="submit">Rechercher</button>
            </span>
        </div>
    </form>';
}

/* Formulaire pour la réponse d'un joueur dans un lobby
 * @param: $lobby les informations du lobby
 */
function vue_reponse_joueur($lobby) {
    echo '<form action="verif_reponse.php?id='.$lobby["id"].'" method="POST" id="reponse_form" class="form-horizontal" autocomplete="off">';
        form_group("text", "Réponse", "reponse", "Entrez votre réponse", "");
        form_group_submit('Envoyer <div id="spinner" style="display: none;"><img src="resources/spinner.gif" alt="spinner" style="margin-top: -3px; width:14px; height:14px;" /></div>', "button_response");
    echo '</form>';
}

/* Formulaire de création d'une partie */
function vue_creation_partie() {
    global $LISTE_CATEGORIES;
    
    echo '
    <form action="creation_partie.php" method="POST" class="form-horizontal">
    
        <div class="form-group">
            <label class="control-label col-sm-3" for="categorie">Catégorie : </label>
            <div class="col-sm-4"> 
                <select class="form-control" name="categorie">';
                foreach($LISTE_CATEGORIES as $categorie)
                    echo '<option value="'. $categorie .'">'. $categorie .'</option>';
            echo '
                </select>
            </div>
        </div>';
        
        form_group_submit("Créer la partie");
        
    echo '</form>';
}
?>