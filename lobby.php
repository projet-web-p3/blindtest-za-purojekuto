<?php
include_once("includes/db.php");
include_once("includes/modele.php");
include_once("includes/vue.php");
update_connexion(); // On se connecte si une session est ouverte


if(!isset($_GET["id"])) { // Si aucun ID de lobby n'est passé en paramètre
    entete('Liste des lobbys');
    if(verif_connexion())
        affiche_info("Pour crééer un lobby, rendez-vous sur l'accueil.");

    // On affiche la liste des lobbys existants
    vue_liste_lobbys(liste_lobbys(), $LISTE_ETATS_LOBBYS);
}
elseif(!ctype_digit($_GET["id"])) { // Si l'ID passé en paramètre n'est pas un entier
    entete('Lobby');
    affiche_erreur("L'ID de lobby entré est incorrect.");
}
else { // Si l'ID passé en paramètre de GET est valide
    if(!verif_connexion()) { // Si l'utilisateur n'est pas connecté
        entete('Lobby');
        affiche_warning("Vous n'êtes pas connecté !");
    }
    else { // Si l'utilisateur est connecté

        // Si aucun lobby ne correspond à l'ID passé en paramètre
        if(!($lobby = get_lobby_contenu($_GET["id"]))) { // Si un lobby associé à cet ID n'existe pas
            entete('Lobby');
            affiche_erreur("Aucun lobby associé à cet ID n'a été trouvé.");
        }
        else { // Si un lobby associé à cet ID existe bien
            entete('Lobby '.$lobby["id"]);
        
            // Si jamais la partie est supprimée
            if(is_lobby_supprime($lobby))
                affiche_erreur("Cette partie est supprimée.");
            else {
                // Si jamais la partie n'est pas finie
                if(!is_lobby_fini($lobby)) {

                    if(isset($_GET["exit"])) { // Si le joueur veut quitter ce lobby
                        // On supprime le joueur
                        if(remove_joueur($lobby["id"], $membre_connecte["id"]))
                            affiche_succes("Vous avez bien quitté ce lobby.");
                        else
                            affiche_warning("Vous n'êtes pas joueur dans ce lobby.");
                    }

                    // Si le joueur veut commencer la partie (donc partie en attente)
                    if(isset($_GET["start"]) && is_lobby_attente($lobby)) {
                        // On vérifie que l'utilisateur est bien créateur du lobby
                        if(is_creator_of_lobby($lobby, $membre_connecte["id"])) {
                            set_lobby_encours($lobby);
                            affiche_succes("Vous avez fait commencer cette partie.");
                        }
                        else
                            affiche_erreur("Vous ne pouvez pas faire commencer cette partie.");
                    }

                    // Si le joueur veut arrêter la partie (donc partie en cours)
                    if(isset($_GET["stop"]) && is_lobby_encours($lobby)) {
                        // On vérifie que l'utilisateur est bien créateur du lobby
                        if(is_creator_of_lobby($lobby, $membre_connecte["id"])) {
                            set_lobby_fini($lobby);
                            affiche_succes("Vous avez fait terminer cette partie.");
                        }
                        else
                            affiche_erreur("Vous ne pouvez pas faire terminer cette partie.");
                    }

                    // Si aucune opération spéciale n'est demandée, on ajoute le membre au lobby
                    if(!isset($_GET["exit"]) && !isset($_GET["start"]) && !isset($_GET["stop"]))
                        if(ajout_joueur($lobby["id"], $membre_connecte["id"]))
                            affiche_succes("Vous avez rejoint ce lobby !");
                }


                // DIV PERMETTANT DE RECUPERER L'ID DU MEMBRE/DU LOBBY EN JAVASCRIPT
                echo '<div id="lobby_id_div" value="'.$lobby["id"].'" style="display: none;"></div>';
                echo '<div id="user_id_div" value="'.$membre_connecte["id"].'" style="display: none;"></div>';


                echo '<h3>Catégorie : '.$lobby["categorie"];

                // BOUTON POUR COMMENCER LA PARTIE
                if(is_creator_of_lobby($lobby, $membre_connecte["id"])) {
                    echo '   <a href="lobby.php?id='.$lobby["id"].'&start" class="btn btn-primary btn-xs" id="button_start_game"';
                    if(!is_lobby_attente($lobby)) echo 'style="display: none;"';
                    echo '>Commencer la partie</a>';
                    echo '<img src="resources/spinner.gif" alt="spinner" style="margin-left: 3px; width:12px; height:12px; display: none;" id="spinner_start_game" style="display: none;" />';
                }

                // BOUTON POUR CHANGER LE CONTENU SUR LA PAGE
                if(is_creator_of_lobby($lobby, $membre_connecte["id"])) {
                    echo '   <a href="#" class="btn btn-primary btn-xs" id="button_change_contenu"';
                    if(!is_lobby_encours($lobby)) echo 'style="display: none;"';
                    echo '>Changer de contenu</a>';
                    echo '<img src="resources/spinner.gif" alt="spinner" style="margin-left: 3px; width:12px; height:12px; display: none;" id="spinner_change_contenu" style="display: none;" />';
                }

                // BOUTON POUR ARRETER LA PARTIE
                if(is_creator_of_lobby($lobby, $membre_connecte["id"])) {
                    echo '   <a href="lobby.php?id='.$lobby["id"].'&stop" class="btn btn-danger btn-xs" id="button_stop_game"';
                    if(!is_lobby_encours($lobby)) echo 'style="display: none;"';
                    echo '>Arrêter la partie</a>';
                    echo '<img src="resources/spinner.gif" alt="spinner" style="margin-left: 3px; width:12px; height:12px; display: none;" id="spinner_stop_game" style="display: none;" />';
                }

                // BOUTON POUR INDIQUER QUI A CREE LA PARTIE
                echo '   <a href="profil.php?id='.$lobby["id_createur"].'" class="btn btn-default btn-xs">Partie créée par '.get_membre($lobby["id_createur"])["pseudo"].'</a>';

                echo '</h3>';


                // AFFICHAGE DU FORMULAIRE DE REPONSE
                if(is_lobby_encours($lobby)) // Si la partie est en cours
                    echo '<div id="lobby_refresh_reponse">';
                else
                    echo '<div id="lobby_refresh_reponse" style="display:none;">';
                vue_reponse_joueur($lobby, !is_lobby_encours($lobby));
                echo '</div>';


                // AFFICHAGE DU CONTENU DE CE LOBBY
                echo '<div class="btn btn-warning" onclick="toggleAudio();" style="float: left; display: none; margin-right: 5px;" id="music_button"><span class="glyphicon glyphicon-pause"></span></div>';
                echo '<div id="lobby_refresh_contenu">';
                afficher_contenu($lobby);
                echo '</div>';


                // AFFICHAGE DE LA LISTE DES JOUEURS
                echo '<div id="lobby_refresh_joueurs">';
                vue_liste_joueurs(get_liste_joueurs($lobby["id"]), $lobby, $membre_connecte); // Affichage de la liste des joueur
                echo '</div>';
            }
        }
    }
}

pied();
?>