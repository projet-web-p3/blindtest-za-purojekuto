<?php
include_once("includes/db.php");
include_once("includes/modele.php");
include_once("includes/vue.php");
update_connexion(); // On se connecte si une session est ouverte

if(!verif_connexion()) // Si l'utilisateur n'est pas connecté
    echo 0;
else { // Si l'utilisateur est connecté
    if(!isset($_GET["id_lobby"]) || !isset($_GET["id_user"])) // Si aucun ID de lobby ou aucun ID de membre n'est passé en paramètre
        echo 0;
    else { // Si un ID de lobby et un ID de membre sont passés en paramètre
        $id_lobby = $_GET["id_lobby"];
        $id_user = $_GET["id_user"];
        
        if(!($lobby = get_lobby_contenu($id_lobby))) // Si le lobby n'existe pas
            echo 0;
        else { // Si le lobby existe bien
            $bonne_reponse = a_donne_la_bonne_reponse($id_lobby, $id_user); // Indique si le joueur peut répondre ou non
            
            // On va renvoyer différentes valeurs possible à JavaScript selon les cas :
            if(is_lobby_encours($lobby) && $bonne_reponse) // Si partie en cours et le joueur a déjà bien répondu
                echo 2;
            elseif(is_lobby_encours($lobby) && !$bonne_reponse) // Si partie en cours et le joueur n'a pas encore bien répondu
                echo 1;
            elseif(is_lobby_fini($lobby)) // Si la partie est finie
                echo 3;
            else // Pour tout autre état
                echo 0;
        }
    }
}
?>