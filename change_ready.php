<?php
include_once("includes/db.php");
include_once("includes/modele.php");
include_once("includes/vue.php");
update_connexion(); // On se connecte si une session est ouverte

// On récupère les informations obtenues en GET
$id_user = $_GET['id_user'];
$id_lobby = $_GET['id_lobby'];
$jsok = $_GET['jsok'];

if(!verif_connexion()) { // Si l'utilisateur n'est pas connecté
    header('Refresh:5; url=index.php'); // Redirection à l'index après 5 secondes
    affiche("Vous n'êtes pas connecté !"); // Réponse AJAX
}

else { // Si l'utilisateur est connecté
    if(!isset($id_lobby) || !isset($id_user)) { // Si au moins l'un des deux ID n'est passé en paramètre
        if($jsok)
            header('Refresh:5; url=index.php'); // Redirection à l'index après 5 secondes
        affiche("Tous les ID doivent être précisés !"); // Réponse AJAX AJAX
    }
    
    elseif(!ctype_digit($id_lobby) || !ctype_digit($id_user)) { // Si un des 2 ID passés en paramètre de GET n'est pas un entier
        if($jsok)
            header('Refresh:5; url=index.php'); // Redirection à l'index après 5 secondes
        affiche("Tous les ID doivent être corrects !"); // Réponse AJAX
    }
    
    else { // Si les deux ID sont corrects
    
        if($membre_connecte["id"] != $id_user) { // Si l'utilisateur essaye de marquer prêt quelqu'un d'autre que lui-même
            if($jsok)
                header('Refresh:5; url=index.php'); // Redirection à l'index après 5 secondes
            affiche("Cet ID n'est pas le vôtre !"); // Réponse AJAX
        }
            
        else { // Si l'utilisateur essaye de se marquer prêt
        
            if(!(get_lobby($id_lobby))) { // Si le lobby n'existe pas
                if($jsok)
                    header('Refresh:5; url=index.php'); // Redirection à l'index après 5 secondes
                affiche("Aucun lobby associé à cet ID n'a été trouvé."); // Réponse AJAX
            }
        
            elseif(!(get_membre($id_user))) { // Si l'utilisateur n'existe pas
                if($jsok)
                    header('Refresh:5; url=index.php'); // Redirection à l'index après 5 secondes
                affiche("Aucun utilisateur associé à cet ID n'a été trouvé."); // Réponse AJAX
            }
                
            else { // Si le lobby et l'utilisateur existent
            
                if(!($joueur = get_joueur($id_user, $id_lobby))) { // Si cet utilisateur n'est pas dans le lobby
                    if($jsok)
                        header('Refresh:5; url=index.php'); // Redirection à l'index après 5 secondes
                    affiche("Vous n'êtes pas joueur de ce lobby !"); // Réponse AJAX
                }
                    
                else { // Si l'utilisateur est bien dans le lobby

                    $db = db_connect();
                    if(is_joueur_ready($joueur)) { // Si le joueur était prêt, on le rend indisponible
                        $rep = db_query($db, "UPDATE jouer SET ready = 'f' WHERE id_membre = ".$id_user." AND id_game = ".$id_lobby.";");
                        affiche("Indisponible"); // Réponse AJAX
                        if($jsok)
                            header('Location: lobby.php?id='.$id_lobby); // Retour au lobby
                    }
                    else { // Si le joueur était indisponible, on le rend prêt
                        $rep = db_query($db, "UPDATE jouer SET ready = 't' WHERE id_membre = ".$id_user." AND id_game = ".$id_lobby.";");
                        affiche("Prêt"); // Réponse AJAX
                        if($jsok)
                            header('Location: lobby.php?id='.$id_lobby); // Retour au lobby
                    }
                    db_close($db);
                    
                }
            }
                
        }
        
    }
}
?>