<?php
include_once("includes/db.php");
include_once("includes/modele.php");
include_once("includes/vue.php");
update_connexion(); // On se connecte si une session est ouverte

if(!verif_connexion()) { // Si l'utilisateur n'est pas connecté
    // Do nothing
}
else { // Si utilisateur connecté
    if(!isset($_GET["id_lobby"])) { // Si aucun ID de lobby n'est fourni
        // Do nothing
    }
    else { // Si un ID de lobby est bien fourni
        $id_lobby = intval($_GET["id_lobby"]); // Récupération de l'ID de lobby
        
        if(!($lobby = get_lobby($id_lobby))) { // Si le lobby n'existe pas
            // Do nothing
        }
        else { // Si le lobby existe
            if(!is_lobby_encours($lobby)) { // Si la partie n'est pas en cours
                // Do nothing
            }
            else { // Si la partie est en cours
                // Si l'utilisateur n'est pas le créateur du lobby
                if(!is_creator_of_lobby($lobby, $membre_connecte["id"]))
                    affiche_erreur("Vous n'êtes pas le créateur de ce lobby et ne pouvez donc pas changer son contenu.");
                else { // Si l'utilisateur est bien le créateur du lobby
                    change_content($lobby); // On change effectivement le contenu de ce lobby
                    set_all_mauvaise_reponse($lobby["id"]); // On indique que tous les joueurs n'ont pas donné la bonne réponse pour ce contenu
                }
            }
        }
    }
}
?>