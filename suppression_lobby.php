<?php
include_once("includes/db.php");
include_once("includes/modele.php");
include_once("includes/vue.php");
update_connexion(); // On se connecte si une session est ouverte

entete("Suppression du lobby ".$_GET["id"]);

if(!verif_connexion()) // Si l'utilisateur n'est pas connecté
    affiche_warning("Vous n'êtes pas connecté !");
else { // Si l'utilisateur est connecté
    if(!isset($_GET["id"])) // Si aucun ID de lobby n'est passé en paramètre
        affiche_erreur("Aucun ID de lobby n'a été spécifié.");
    elseif(!ctype_digit($_GET["id"])) // Si l'ID passé en paramètre de GET n'est pas un entier
        affiche_erreur("L'ID lobby entré est incorrect.");
    else { // Si l'ID passé en paramètre de GET est bien un entier
        if(!($lobby = get_lobby($_GET["id"]))) // Si aucun lobby ne correspond à l'ID passé en paramètre
            affiche_erreur("Aucun lobby associé à cet ID n'a été trouvé.");
        else { // Si on a bien trouvé un lobby avec cet ID
            if(is_lobby_supprime($lobby)) // Si le lobby est déjà supprimé
                affiche_erreur("La partie est déjà supprimée.");
            elseif(is_lobby_fini($lobby)) // Si le lobby est fini
                affiche_erreur("Cette partie est terminée. Vous ne pouvez pas la supprimer.");
            elseif(is_lobby_encours($lobby)) // Si le lobby est "en cours"
                affiche_erreur("Cette partie est en cours. Vous ne pouvez pas la supprimer.");
            else { // Si la partie est "en attente"
                if(!is_creator_of_lobby($lobby, $membre_connecte["id"]) && !is_membre_admin($membre_connecte)) // Si l'user n'est ni le créateur du lobby, ni admin
                    affiche_erreur("Vous n'êtes pas le créateur de cette partie. Vous ne pouvez donc pas la supprimer.");
                else {
                    suppression_lobby($lobby);
                    affiche_succes("La partie a été correctement supprimée.");
                }
            }
        }
    }
}

pied();
?>