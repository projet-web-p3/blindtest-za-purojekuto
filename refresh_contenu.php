<?php
include_once("includes/db.php");
include_once("includes/modele.php");
include_once("includes/vue.php");
update_connexion(); // On se connecte si une session est ouverte

if(!verif_connexion()) // Si l'utilisateur n'est pas connecté
    affiche_erreur("Vous n'êtes plus connecté.");
else { // Si l'utilisateur est connecté
    if(!isset($_GET["id_lobby"])) // Si aucun ID lobby passé en paramètre
        affiche_erreur("Un problème est survenu.");
    elseif(!ctype_digit($_GET["id_lobby"])) // Si l'ID passé en paramètre n'est pas un entier
        affiche_erreur("Un problème est survenu.");
    else { // Si un ID de lobby est bien passé en paramètre
        $id_lobby = $_GET["id_lobby"];
        
        if(!($lobby = get_lobby_contenu($id_lobby))) // Si le lobby n'existe pas
            affiche_erreur("Ce lobby n'existe pas.");
        elseif(is_lobby_supprime($lobby)) // Si le lobby est supprimé
            affiche_erreur("Ce lobby est supprimé.");
        elseif(is_lobby_fini($lobby)) // Si le lobby est supprimé
            affiche_info("La partie est finie.");
        else { // Si le lobby existe bien
            if(!is_joueur_in_lobby($id_lobby, $membre_connecte["id"])) // Si le joueur n'est pas dans le lobby
                affiche_erreur("Vous n'êtes pas joueur dans ce lobby.");
            else // Si le joueur est bien dans le lobby
                afficher_contenu($lobby);
        }
    }
}
?>