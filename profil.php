<?php
include_once("includes/db.php");
include_once("includes/modele.php");
include_once("includes/vue.php");
update_connexion(); // On se connecte si une session est ouverte

if(!isset($_GET["id"])) { // Si aucun ID d'utilisateur n'est passé en paramètre
    if(verif_connexion()) // Si l'utilisateur est connecté, on le redirige sur son profil
        header('Location: profil.php?id='.$membre_connecte["id"]);
    else { // Si l'utilisateur n'est pas connecté, on affiche un message d'erreur
        entete("Profil");
        affiche_erreur("Vous n'êtes pas connecté et aucun ID d'utilisateur n'a été spécifié.");
    }
}

elseif(!ctype_digit($_GET["id"])) { // si l'ID passé en paramètre de GET n'est pas un entier
    entete("Profil");
    affiche_erreur("L'ID d'utilisateur entré est incorrect.");
}

else { // Si un ID de membre valide est passé en paramètre
    if($membre = get_membre($_GET["id"])) { // Si l'utilisateur existe, on affiche son profil
        if(verif_connexion() && ($membre["id"] == $membre_connecte["id"])) { // Si l'utilisateur est connecté et que c'est sa page
            
            // Si un formulaire de modification de profil a été rempli
            if(isset($_POST["edit_profil_form"])) {
                // Récupération des valeurs des champs
                $pseudo = secure_user_input($_POST["pseudo"]);
                $email = secure_user_input($_POST["email"]);
                $newPassword = $_POST["newPassword"];
                $newPasswordConfirm = $_POST["newPasswordConfirm"];
                $password = $_POST["password"];
                
                // Gestion des erreurs
                $errors["empty_form"] = !($newPassword || $pseudo || $email); // Si aucune modification n'est rentrée
                $errors["wrong_pwd"] = !verif_mdp($membre_connecte["id"], $password); // Si le mot de passe actuel entré est faux
                $errors["failure_conf_pwd"] = ($newPassword != $newPasswordConfirm); // Si la confirmation est erronnée
                $errors["failure_pseudo"] = !is_unique_pseudo($pseudo);  // Vérification de l'unicité du pseudo
                $errors["failure_email"] = ($email && !filter_var($email, FILTER_VALIDATE_EMAIL)); // Vérification du format de l'adresse mail
                $errors["failure_email_exists"] = !is_unique_mail($email); // Vérification de l'unicité du mail
                
                // Mise à jour des champs si des erreurs sont constatées
                $newPassword = $errors["failure_conf_pwd"] ? "" : $newPassword; // Si la confirmation est fausse, pas de changement de MDP
                $pseudo = $errors["failure_pseudo"] ? "" : $pseudo; // Si le pseudo est déjà pris, pas de changement de pseudo
                $email = ($errors["failure_email"] || $errors["failure_email_exists"]) ? "" : $email; // Si l'adresse mail est déjà utilisée, par de changement d'adresse mail
                
                // On effectue les modifications si le mot de passe est bon et au moins une modification est demandée
                if(!$errors["wrong_pwd"] && !$errors["empty_form"])
                    $update_status = update_membre($membre_connecte["id"], $pseudo, $newPassword, $email);
                
                update_connexion(); // On met à jour la connexion (changement de MDP, par exemple)
                $membre = get_membre($_GET["id"]); // On met bien à jour
            }
            
            entete("Votre profil");
            
            if(isset($_POST["edit_profil_form"])) {
                // On affiche les modifications effectuées lors de la modification de profil
                if($update_status["pseudo"] == 1)
                    affiche_succes("Modification du pseudo effectuée.");
                if($update_status["password"] == 1) {
                    affiche_succes("Modification du mot de passe effectuée.");
                    affiche_info("Vous devez vous reconnecter pour continuer.");
                }
                if($update_status["email"] == 1)
                    affiche_succes("Modification du mail effectuée.");
                
                // On affiche les erreurs qui ont eu lieu lors de la modification de profil
                if($errors["empty_form"])
                    affiche_warning("Veuillez remplir au moins un champ.");
            }
        }
        else
            entete("Profil de ".$membre["pseudo"]);
        
        if(is_membre_banni($membre)) // Si le membre est banni, on l'indique
            affiche_erreur("Ce membre est banni.");
        if(is_membre_admin($membre)) // Si le membre est admin, on l'indique
            affiche_info("Ce membre est administrateur.");
        
        vue_profil($membre);
        
        // Si c'est le profil du membre connecté, on lui propose aussi la modification de ses données
        if(verif_connexion() && ($membre["id"] == $membre_connecte["id"])) {
            echo '<h3>Modification de profil</h3>';
            echo '<p>Laissez un champ vide si vous ne voulez pas le modifier.</p>';
            vue_edition_profil($membre_connecte, $errors);
        }
        
    }
    else { // Si l'utilisateur n'existe pas, on affiche une erreur
        entete("Profil");
        affiche_erreur("Aucun utilisateur associé à cet ID n'a été trouvé.");
    }
}

pied();
?>