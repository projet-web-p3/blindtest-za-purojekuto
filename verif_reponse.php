<?php
include_once("includes/db.php");
include_once("includes/modele.php");
include_once("includes/vue.php");
update_connexion(); // On se connecte si une session est ouverte

if(!verif_connexion()) { // Si l'utilisateur n'est pas connecté
    // Do nothing
}
else { // Si utilisateur connecté
    if(!isset($_GET["id_lobby"]) || !isset($_GET["id_membre"]) || !isset($_POST["reponse"])) { // Si aucun ID de lobby ni ID de membre sont fournis, ni une réponse
        // Do nothing
    }
    else { // Si un ID de lobby et un ID de membre sont bien fournis, ainsi qu'une réponse
        $id_lobby = intval($_GET["id_lobby"]); // Récupération de l'ID de lobby
        $id_membre = intval($_GET["id_membre"]); // Récupération de l'ID de membre
        $reponse = secure_user_input($_POST["reponse"]); // Récupération de la réponse de l'utilisateur
        
        if(!($lobby = get_lobby_contenu($id_lobby))) { // Si le lobby n'existe pas
            // Do nothing
        }
        else { // Si le lobby existe bien
            if(!is_lobby_encours($lobby)) { // Si la partie n'est pas en cours
                // Do nothing
            }
            else { // Si la partie est en cours
                if(!is_joueur_in_lobby($id_lobby, $id_membre)) { // Si le joueur n'est pas dans le lobby
                    // Do nothing
                }
                else { // Si le joueur est bien dans le lobby
                    $distance = levenshtein(strtolower($lobby["reponse_attendue"]), strtolower($reponse), 2, 1, 2) * 0.5;
                    $longueur_reponse = strlen($lobby["reponse_attendue"]);
                    
                    $pourcentage_reponse = (($longueur_reponse-$distance)/$longueur_reponse)*100;
                    
                    // On rajoute 1 point si le pourcentage de correspondance est > 75%
                    if($pourcentage_reponse > 75)
                        $score = 1;
                    else
                        $score = 0;
                        
                    echo $score;
                    
                    // On ajoute des points si le joueur peut répondre et que sa réponse est acceptée
                    if(!$joueur["a_donne_la_bonne_reponse"] && $score > 0) {
                        add_score(get_joueur($id_membre, $id_lobby), $score);
                        set_true_a_donne_la_bonne_reponse($id_lobby, $id_membre); // On interdit maintenant une nouvelle réponse
                    }
                }
            }
        }
    }
}
?>